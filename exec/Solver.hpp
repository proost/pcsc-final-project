//
// Created by Anna  Proost on 11/12/2023.
//

#ifndef PCSC_FINAL_PROJECT_SOLVER_HPP
#define PCSC_FINAL_PROJECT_SOLVER_HPP

#include <string>
#include <vector>
#include "Eigen/Dense"

#include "../inputs/headers.hpp"
#include "../methods/headers.hpp"
#include "../outputs/Solution.hpp"
#include "../exec/headers.hpp"

/**
 * @brief Solver is a class used to provide abstraction to the `central_exec` and `test_suites` files,
 * by encapsulating all details of instantiating and using Functions & Methods.
 * Simply said, it is an interface towards any higher-level files to interact with the rest of the project.
 *
 * There are a number of functions defined for different goals, the most important of which are the `*Method()` functions.
 * These do all the work of finding the root of a non-linear problem from start to finish: they instantiate the relevant
 * Functions/System and Methods, call upon the calculation and return a 'Solution' class which has all info one might need
 * about the result of the method.
 *
 * Besides this, there are a few simple functions to evaluate Functions given only a string, as well as functions that
 * provide a clean & consistent way of printing the Solutions & all info inside it in the terminal, for the user to see.
 *
 * There is no constructor provided in order to be able to use any instance of the `Solver`class as broadly as possible. We simply
 * define a `Solver solver`at the beginning of the high-level file or test, and can then use it for whatever it is needed specifically.
 */
class Solver {

public:

    /**
      * @brief Solves a non-linear problem based on the parameters specified in the provided configuration file.
      */
    Solution Solve(ConfigFileReader file);

    /**
     * @brief Evaluates a mono-variable function for a specific value of `x`.
     */
    double EvaluateFunction(const std::string &eq, double x);

    /**
     * @brief Evaluates a multi-variable function for specific values of its variables (`x, y, ...`).
     */
    double EvaluateMultiFunction(const std::string &eq, Eigen::VectorXd x, const std::vector<std::string> &var);

    /**
      * @brief Creates a MonoFunction object based on the configuration provided in the `ConfigFileReader` class.
      */
    MonoFunction InputFunction(ConfigFileReader file);

    /**
      * @brief Creates a System object based on the configuration provided in the `ConfigFileReader` class.
      */
    System InputSystem(ConfigFileReader file);

    /**
     * @brief Applies the non-accelerated `Newton` method to find a root of a given mono-variable equation for given conditions.
     */
    Solution NewtonMethod(const std::string &eq, const std::string &der, double start_point, double stop_tol, int max_iter);

    /**
     * @brief Applies the accelerated `Newton` method to find a root of a given mono-variable equation for given conditions.
     */
    Solution NewtonMethodA(const std::string &eq, const std::string &der, double start_point, double stop_tol, int max_iter);

    /**
     * @brief Applies the `Newton` method for Systems to find a root of a given `System of multi-variable functions for given conditions.
     */
    Solution SystemNewtonMethod(const std::vector<std::string> &eqs, const std::vector<std::vector<std::string>> &der,
                                const std::vector<std::string> &var, const std::vector<double> &start_points,
                                double stop_tol, int max_iter);


    /**
     * @brief Applies the non-accelerated `Fixed Point` method to find a root of a given mono-variable equation for given conditions.`
     */
    Solution FixedPointMethod(const std::string &eq, const std::string &fp, double start_point, double stop_tol0, int max_iter);

    /**
     * @brief Applies the accelerated `Fixed Point` method to find a root of a given mono-variable equation for given conditions.
     */
    Solution FixedPointMethodA(const std::string &eq, const std::string &fp, double start_point, double stop_tol0, int max_iter);

    /**
     * @brief Applies the `Chord` method to find a root of a given mono-variable equation for given conditions.
     */
    Solution ChordMethod(const std::string &eq, std::vector<double> start_point, double stop_tol, int max_iter);

    /**
     * @brief Applies the `Bisection` method to find a root of a given mono-variable equation for given conditions.
     */
    Solution BisectionMethod(const std::string &eq, std::vector<double> start_point, double stop_tol, int max_iter);

};


#endif //PCSC_FINAL_PROJECT_SOLVER_HPP
