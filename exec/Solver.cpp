//
// Created by Anna  Proost on 11/12/2023.
//

#include <fstream>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <numeric>
#include <iomanip>

#include "../inputs/headers.hpp"
#include "../methods/headers.hpp"
#include "../exec/headers.hpp"
#include "../outputs/Solution.hpp"
#include "Solver.hpp"

/**
  * @brief Evaluates a mono-variable function for a specific value of `x`.
  *
  * Instantiates a MonoFunction instance for the given equation (no need for `der` or `eq_fp`, as those will not be relevant),
  * and then evaluates the function for the given value of `x` by overloading the () operator.
  *
  * @param eq the equation we want to evaluate in string form
  * @param x the value we want to evaluate the function for
  *
  * @return a double which is the function evaluation for the value `x`.
  */
double Solver::EvaluateFunction(const std::string& eq, double x) {

    MonoFunction Eq(eq, "", "");
    return Eq(x);
}

/**
  * @brief Evaluates a multi-variable function for specific values of its variables (`x, y, ...`).
  *
  * Instantiates a MultiFunction instance for the given equation (no need for `der`, as this will not be relevant),
  * and then evaluates the function for the given values of its variables by overloading the () operator.
  *
  * @param eq the equation we want to evaluate in string form
  * @param x the values we want to evaluate the function for
  * @param var the order of the values (first element of `x` maps onto first element, second on second, ...)
  *
  * @return a double which is the function evaluation for the values in `x`.
  */
double Solver::EvaluateMultiFunction(const std::string& eq, Eigen::VectorXd x, const std::vector<std::string>& var) {
    MultiFunction Eq(eq, std::vector<std::string>(var.size(), ""), var);
    return Eq(x);
}


/**
  * @brief Solves a non-linear problem based on the parameters specified in the provided configuration file.
  *
  * This method reads the chosen solving method, algorithmic settings, and other parameters from the configuration file `file
  * and uses them to select and invoke the corresponding solving method. It returns a `Solution` object containing information
  * about the result of the solving process.
  *
  * @param file The `ConfigFileReader` class instance containing input parameters from the configuration file.
  * @return A `Solution` object containing information about the result of the solving process.
  *
  * @note Make sure the configuration file provides valid input parameters for the selected solving method.
  */
Solution Solver::Solve(ConfigFileReader file) {

        if (file.getMethod() == "N"){

            if (file.getAitken()) {

                return NewtonMethodA(file.getFunction(), file.getDerivative(), file.getStartPoint(), file.getTolerance(), file.getIterations());

            } else {
                return NewtonMethod(file.getFunction(), file.getDerivative(), file.getStartPoint(), file.getTolerance(), file.getIterations());
            }
        }
        else if (file.getMethod() == "FP"){

            if (file.getAitken()) {

                return FixedPointMethodA(file.getFunction(), file.getFixedPoint(), file.getStartPoint(), file.getTolerance(), file.getIterations());

            } else {
                return FixedPointMethod(file.getFunction(), file.getFixedPoint(), file.getStartPoint(), file.getTolerance(), file.getIterations());
            }

        }

        else if (file.getMethod() == "C") {

            return ChordMethod(file.getFunction(), file.getStartPoints(), file.getTolerance(), file.getIterations());

        }

        else if (file.getMethod() == "B") {

            return BisectionMethod(file.getFunction(), file.getStartPoints(), file.getTolerance(), file.getIterations());

        }

        else if (file.getMethod() == "SN"){

            return SystemNewtonMethod(file.getFunctions(), file.getDerivatives(), file.getVariables(), file.getStartPoints(), file.getTolerance(), file.getIterations()) ;
        }

       else {
            throw std::runtime_error("Error: Unsupported method type");
            //std::cerr << "\033[1;31mIt seems like your inputs do not match up. If unsure about what's wrong and what fields should/shouldn't be filled in, please consult the section 'conventions & rules to follow' in the README.md.\033[0m" << std::endl;
            //return sol;
    }
}


/**
 * @brief Creates a MonoFunction object based on the configuration provided in the `ConfigFileReader` class.
 *
 * This function takes a ConfigFileReader instance as input, extracts the necessary information from it,
 * and then creates a MonoFunction object if the method specified in the configuration is one of the valid ones (`N`, `FP`, `C` or `B`).
 *
 * @param file A `ConfigFileReader` object containing the configuration data & all needed arguments.
 * @return A MonoFunction object initialized with the functions/derivatives/fixed point equations specified in the configuration,
 * based on the method & the information it needs.
 *
 * @note The ConfigFileReader should provide information the info about functions/derivatives/ required to create the MonoFunction
 * in the correct way (see `README`).
 */

MonoFunction Solver::InputFunction(ConfigFileReader file) {

    if (file.getMethod() == "N"){ return MonoFunction(file.getFunction(), "", file.getDerivative()); }
    else if (file.getMethod() == "FP") { return MonoFunction(file.getFunction(), file.getFixedPoint(), "");}
    else if (file.getMethod() == "C" || file.getMethod() == "B") { return MonoFunction(file.getFunction(), "", "");}

    else {throw std::runtime_error("Error: Unsupported method type");}
}


/**
 * @brief Creates a System object based on the configuration provided in the `ConfigFileReader` class.
 *
 * This function takes a ConfigFileReader instance as input, extracts the necessary information from it,
 * and then creates a System object if the method specified in the configuration is `SN`.
 *
 * @param file A `ConfigFileReader` object containing the configuration data & all needed arguments.
 * @return A System object initialized with the functions, derivatives and variables specified in the configuration.
 *
 * @note The ConfigFileReader should provide information the info about functions, derivatives, and variables required to create the System
 * in the correct way (see `README`).
 */
System Solver::InputSystem(ConfigFileReader file) {

    if (file.getMethod() == "SN"){

        std::vector<MultiFunction> functions;

        functions.reserve((int)file.getFunctions().size());
        for (int i = 0; i < (int)file.getVariables().size(); i++) { functions.push_back( MultiFunction(file.getFunctions()[i], file.getDerivatives()[i], file.getVariables()) ); }

        System sys(functions);

        return sys;
    }

    else {throw std::runtime_error("Error: Unsupported method type");}
}

/**
  * @brief Applies the non-accelerated Newton method to find a root of a given mono-variable equation for given conditions.
  *
  * This function will initialize the `MonoFunction` with the given `equation` & `der`, then perform the `Newton` method on it
  * for the given conditions (`max_iter`, `tolerance`, `start_point`). It returns a `Solution` class which encapsulates all
  * relevant & interesting information about the result of the method.
  *
  * @param eq the equation of the `MonoFunction` we want to find a root of.
  * @param der the derivative of the `MonoFunction` we want to find a root of.
  * @param start_point the point from which the `Newton` method will start to iterate.
  * @param stop_tol the tolerance at which the `Newton` method considers its sequence to be converged.
  * @param max_iter the maximal iterations the `Newton` method will perform before returning, converged or not.
  *
  * @return a `Solution` class with all information about the method's result.
  */
Solution Solver::NewtonMethod(const std::string& eq, const std::string& der, double start_point, double stop_tol, int max_iter) {

    MonoFunction Eq(eq, "", der);
    Method* p = Method::getMethod("N",false);

    Solution sol = p->operator()(Eq,start_point,stop_tol,max_iter);
    return sol;

}

/**
  * @brief Applies the accelerated `Newton` method to find a root of a given mono-variable equation for given conditions.
  *
  * This function will initialize the `MonoFunction` with the given `equation` & `der`, then perform the Accelerated `Newton` method on it
  * for the given conditions (`max_iter`, `tolerance`, `start_point`), by instantiating the `Aitken` method with this `Newton` class .
  * It returns a `Solution` class which encapsulates all relevant & interesting information about the result of the method.
  *
  * @param eq the equation of the `MonoFunction` we want to find a root of.
  * @param der the derivative of the `MonoFunction` we want to find a root of.
  * @param start_point the point from which the `Aitken` ( & `Newton`) method will start to iterate.
  * @param stop_tol the tolerance at which the `Aitken` method considers its sequence to be converged.
  * @param max_iter the maximal iterations the `Aitken` method will perform before returning, converged or not.
  *
  * @return a `Solution` class with all information about the method's result.
  */
Solution Solver::NewtonMethodA(const std::string& eq, const std::string& der, double start_point, double stop_tol, int max_iter) {

    MonoFunction Eq(eq, "", der);
    Method* p = Method::getMethod("N",true);

    Solution sol = p->operator()(Eq,start_point,stop_tol,max_iter);
    return sol;

}

/**
  * @brief Applies the `Newton` method for Systems to find a root of a given `System` of multi-variable functions for given conditions.
  *
  * This function will initialize the `MultiFunction`'s with the given `equation`'s & `der`'s, then perform the `Newton` method for Systems on it
  * for the given conditions (`max_iter`, `tolerance`, `start_point`). It returns a `Solution` class which encapsulates all
  * relevant & interesting information about the result of the method.
  *
  * @param eqs the equations of the `MultiFunction`'s in the `System` we want to find a root of.
  * @param der the Jacobian of the system of equations we want to find a root of.
  * @param start_points the vector from which the `Newton` method will start to iterate.
  * @param stop_tol the tolerance at which the `Newton` method considers its sequence to be converged.
  * @param max_iter the maximal iterations the `Newton` method will perform before returning, converged or not.
  *
  * @return a `Solution` class with all information about the method's result.
  */
Solution Solver::SystemNewtonMethod(const std::vector<std::string>& eqs, const std::vector<std::vector<std::string>>& der, const std::vector<std::string>& var, const std::vector<double>& start_points, double stop_tol, int max_iter) {

    std::vector<MultiFunction> functions;

    functions.reserve((int)eqs.size());
    for (int i = 0; i < (int)var.size(); i++) { functions.push_back( MultiFunction(eqs[i], der[i], var) ); }

    System syst(functions);

    Method* p = Method::getMethod("SN",false);

    Solution sol = p->operator()(syst,start_points,stop_tol,max_iter);
    return sol;

}

/**
  * @brief Applies the non-accelerated `Fixed Point` method to find a root of a given mono-variable equation for given conditions.
  *
  * This function will initialize the `MonoFunction` with the given `equation` & `fixed point equation`, then perform the `Fixed Point` method on it
  * for the given conditions (`max_iter`, `tolerance`, `start_point`). It returns a `Solution` class which encapsulates all
  * relevant & interesting information about the result of the method.
  *
  * @param eq the equation of the `MonoFunction` we want to find a root of.
  * @param fp the fixed-point equation `g(x)` such that `f(x) = g(x) - x`
  * @param start_point the point from which the `Fixed Point` method will start to iterate.
  * @param stop_tol the tolerance at which the `Fixed Point` method considers its sequence to be converged.
  * @param max_iter the maximal iterations the `Fixed Point` method will perform before returning, converged or not.
  *
  * @return a `Solution` class with all information about the method's result.
  */
Solution Solver::FixedPointMethod(const std::string& eq, const std::string& fp, double start_point, double stop_tol, int max_iter) {

    MonoFunction Eq(eq, fp, "");

    Method* p = Method::getMethod("FP",false);

    Solution sol = p->operator()(Eq,start_point,stop_tol,max_iter);
    return sol;

}

/**
  * @brief Applies the accelerated `Fixed Point` method to find a root of a given mono-variable equation for given conditions.
  *
  * This function will initialize the `MonoFunction` with the given `equation` & `fixed-point equation`, then perform the Accelerated `Fixed Point` method on it
  * for the given conditions (`max_iter`, `tolerance`, `start_point`), by instantiating the `Aitken` method with this `Fixed Point` class.
  * It returns a `Solution` class which encapsulates all relevant & interesting information about the result of the method.
  *
  * @param eq the equation of the `MonoFunction` we want to find a root of.
  * @param fp the fixed-point equation `g(x)` such that `f(x) = g(x) - x`
  * @param start_point the point from which the `Aitken` ( & `Fixed Point`) method will start to iterate.
  * @param stop_tol the tolerance at which the `Aitken` method considers its sequence to be converged.
  * @param max_iter the maximal iterations the `Aitken` method will perform before returning, converged or not.
  *
  * @return a `Solution`class with all information about the method's result.
  */
Solution Solver::FixedPointMethodA(const std::string& eq, const std::string& fp, double start_point, double stop_tol, int max_iter) {

    MonoFunction Eq(eq, fp, "");

    Method* p = Method::getMethod("FP",true);

    Solution sol = p->operator()(Eq,start_point,stop_tol,max_iter);
    return sol;

}

/**
  * @brief Applies the `Chord` method to find a root of a given mono-variable equation for given conditions.
  *
  * This function will initialize the `MonoFunction` with the given `equation` & `der`, then perform the `Chord` method on it
  * for the given conditions (`max_iter`, `tolerance`, `start_point`). It returns a `Solution` class which encapsulates all
  * relevant & interesting information about the result of the method.
  *
  * @param eq the equation of the `MonoFunction` we want to find a root of.
  * @param start_point the 2 first points/iterations from which the `Chord` method will start to iterate.
  * @param stop_tol the tolerance at which the `Chord` method considers its sequence to be converged.
  * @param max_iter the maximal iterations the `Chord` method will perform before returning, converged or not.
  *
  * @return a `Solution` class with all information about the method's result.
  */
Solution Solver::ChordMethod(const std::string& eq, std::vector<double> start_point, double stop_tol, int max_iter) {

    MonoFunction Eq(eq, "", "");

    Method* p = Method::getMethod("C",false);

    Solution sol = p->operator()(Eq,start_point,stop_tol,max_iter);
    return sol;

}

/**
  * @brief Applies the `Bisection` method to find a root of a given mono-variable equation for given conditions.
  *
  * This function will initialize the `MonoFunction` with the given `equation`, then perform the `Bisection` method on it
  * for the given conditions (`max_iter`, `tolerance`, `start_point`). It returns a `Solution` class which encapsulates all
  * relevant & interesting information about the result of the method.
  *
  * @param eq the equation of the `MonoFunction` we want to find a root of.
  * @param start_point the interval of points from which the `Bisection` method will start to iterate.
  * @param stop_tol the tolerance at which the `Bisection` method considers its sequence to be converged.
  * @param max_iter the maximal iterations the `Bisection` method will perform before returning, converged or not.
  *
  * @return a `Solution` class with all information about the method's result.
  */
Solution Solver::BisectionMethod(const std::string& eq, std::vector<double> start_point, double stop_tol, int max_iter) {

    MonoFunction Eq(eq, "", "");


    Method* p = Method::getMethod("B",false);

    Solution sol = p->operator()(Eq,start_point,stop_tol,max_iter);
    return sol;

}




