//
// Created by Anna  Proost on 06/12/2023.
//

#ifndef PCSC_FINAL_PROJECT_READFILE_HPP
#define PCSC_FINAL_PROJECT_READFILE_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <variant>

/**
 * @brief Enum representing the keys used in the configuration file for specifying different parameters.
 */
enum ConfigKey {
    FUNCTION,
    VARIABLES,
    FIXED_POINT,
    DERIVATIVE,
    ALGORITHM,
    AITKEN,
    TOLERANCE,
    MAX_ITERATIONS,
    START_POINT
};


/**
 * @brief `ConfigFileReader` is a class responsible for reading in and interpreting `.txt` configuration files
 * used to provide input parameters for the solving of non-linear problems.
 *
 * It reads the config file at the provided path, parses it and uses an enumeration with keys to save the values.
 * Methods are provided to retrieve specific parameters such as the function equation,
 * fixed-point equation, derivative equation, chosen algorithm, Aitken acceleration status, tolerance,
 * maximum iterations, and start points.
 *
 * It was chosen to make the `ConfigFileReader` its own class for two main reasons:
 * - instantiating an instance `read_file` of this class at the beginning of the `central_exec` file provides an abstraction & a very elegant way
 * of accessing the different input parameters later on, by simply calling `read_file.get*()`when needing a specific parameter.
 *
 * - It also allows for the possibility of other parsing other file types (e.g. JSON) later on, without having to rewrite and copy
 * large parts of the code. all Getters get to stay the same, and we would simply add another `readConfigFile()` function that can
 * parse the new file type.
 *
 */
class ConfigFileReader {

public:

    std::vector<std::pair<ConfigKey, std::string>> elements;

    /**
     * @brief Constructor for ConfigFileReader
     */
    explicit ConfigFileReader(const std::string& filePath);



    /**
     * @brief Getter for the function equation string.
     */
    std::string getFunction() const;

    /**
     * @brief Getter for the fixed-point equation string.
     */
    std::string getFixedPoint() const;

    /**
     * @brief Getter for the derivative string.
     */
    std::string getDerivative() const;

    /**
     * @brief Getter for the method string.
     */
    std::string getMethod() const;

    /**
     * @brief Getter for the Aitken bool.
     */
    bool getAitken() const;

    /**
     * @brief Getter for the tolerance double.
     */
    double getTolerance() const;

    /**
     * @brief Getter for the iterations int.
     */
    int getIterations() const;

    /**
     * @brief Getter for the startpoint (double)
     */
    double getStartPoint() const;

    /**
     * @brief Getter for the startpoints (vector of doubles)
     */
    std::vector<double> getStartPoints() const;

    /**
     * @brief Getter for multiple function equations, only accessible if method = Newton method for Systems
     */
    std::vector<std::string> getFunctions() const;

    /**
     * @brief Getter for all variables, only accessible if method = Newton method for Systems
     */
    std::vector<std::string> getVariables() const;

    /**
     * @brief Getter for all derivatives, only accessible if method = Newton method for Systems
     */
    std::vector<std::vector<std::string>> getDerivatives() const;


private:
    std::string filePath;

    /**
     * @brief Gets the value associated with a specific key in the configuration file (accesses enum)
     */
    std::string getValueByKey(ConfigKey key) const;

    /**
     * @brief Reads the configuration file and returns a vector of key-value pairs.
     */
    std::vector<std::pair<ConfigKey, std::string>> readConfigFile() const;
};

#endif //PCSC_FINAL_PROJECT_READFILE_HPP
