#include "ReadFile.hpp"
#include "../inputs/headers.hpp"
#include "../methods/headers.hpp"

/**
  * @brief Reads the configuration file and returns a vector of key-value pairs.
  *
  * This function opens the file at the given path and reads its content line by line. Each non-empty line except for the ones
  * starting with # is parsed into a key-value pair with the key being specified in the file as the first word,
  * and the value is the remaining part of the line after the '=' sign. The keys are case-insensitive.
  *
  * we recognize the following keys:
  * - `function`: Represents the main function equation, or equations if method = Newton method for Systems.
  * - `variables`: Represents the variables involved in the function, only relevant if method = Newton method for Systems.
  * - `fixed_point`: Represents the fixed-point equation for iterative methods, only relevant if method = Fixed Point method
  * - `derivative`: Represents the derivative equation for certain methods, only relevant if method = Newton method (for Systems).
  * - `algorithm`: Represents the chosen algorithm/method for solving the problem.
  * - `aitken`: Represents the Aitken acceleration status as a boolean (true or false), only relevant if method = Newton method or Fixed Point method
  * - `tolerance`: Represents the tolerance for stopping the iteration.
  * - `max_iterations`: Represents the maximum number of iterations.
  * - `start_point`: Represents the initial guess for the root or starting point(s).
  *
  *  If an unrecognized key is encountered, an error message is printed, and the key (line) is skipped.
  *
  *  @return A vector of key-value pairs, where each pair represents a configuration element from the file.
  *  If the file cannot be opened or an error occurs, an empty vector is returned.
  *
  */
std::vector<std::pair<ConfigKey, std::string>> ConfigFileReader::readConfigFile() const {
    std::vector<std::pair<ConfigKey, std::string>> all_elements;

    std::ifstream file(filePath);

    if (!file.is_open()) {
        return elements;
    }

    std::string line;
    while (std::getline(file, line)) {
        if (!line.empty() && line[0] != '#') {
            std::istringstream iss(line);
            std::string key, value;
            if (iss >> key >> value) {
                std::transform(key.begin(), key.end(), key.begin(), ::tolower);
                ConfigKey configKey;
                if (key == "function") {
                    configKey = FUNCTION;
                } else if (key == "variables") {
                    configKey = VARIABLES;
                } else if (key == "fixed_point") {
                    configKey = FIXED_POINT;
                } else if (key == "derivative") {
                    configKey = DERIVATIVE;
                } else if (key == "algorithm") {
                    configKey = ALGORITHM;
                } else if (key == "aitken") {
                    configKey = AITKEN;
                } else if (key == "tolerance") {
                    configKey = TOLERANCE;
                } else if (key == "max_iterations") {
                    configKey = MAX_ITERATIONS;
                } else if (key == "start_point") {
                    configKey = START_POINT;
                } else {
                    std::cerr << "Invalid key: " << key << std::endl;
                    continue;
                }

                // Skip the "="
                iss.ignore(1, '=');

                // Read the rest of the line as the value
                std::getline(iss, value);

                // Store the key and value pair
                all_elements.emplace_back(configKey, value);
            }
        }
    }

    file.close();
    return all_elements;
}

/**
 * @brief Constructor for ConfigFileReader
 *
 * Constructs a `ConfigFileReader` object with the provided file path and reads the configuration file.
 *
 * @param filePath The path to the configuration file.
 */

ConfigFileReader::ConfigFileReader(const std::string& filePath) : filePath(filePath) {
    elements = readConfigFile();
}

/**
 * @brief Gets the value associated with a specific key in the configuration file (accesses enum).
 *
 * @param key The key for which we want to retrieve the value.
 * @return The value associated with the given key. If the key is not found, we return "Invalid key."
 */
std::string ConfigFileReader::getValueByKey(ConfigKey key) const {
    auto it = std::find_if(elements.begin(), elements.end(),
                           [key](const auto& element) { return element.first == key; });

    if (it != elements.end()) {
        return it->second;
    } else {
        return "Invalid key.";
    }
}

/**
  * @brief Getter for the function equation string.
  *
  * @return The main function or equation.
  */
std::string ConfigFileReader::getFunction() const {
    return getValueByKey(FUNCTION);
}

/**
  * @brief Getter for the fixed-point equation string.
  *
  * @return The fixed-point equation.
  */
std::string ConfigFileReader::getFixedPoint() const {
    if (getMethod() != "FP") {
        throw std::invalid_argument("This method " + getValueByKey(ALGORITHM) + " does not have a fixed point equation that needs to be inputted\n");
    }
    return getValueByKey(FIXED_POINT);
}

/**
  * @brief Getter for the derivative string.
  *
  * @return The derivative.
  */
std::string ConfigFileReader::getDerivative() const {
    if (getMethod() != "N") {
        throw std::invalid_argument("This method " + getValueByKey(ALGORITHM) + " does not have a derivative that needs to be inputted\n");
    }

    return getValueByKey(DERIVATIVE);
}

/**
  * @brief Getter for the method string.
  *
  * If the method is not one of the ones we implemented in the project, an error is thrown.
  *
  * @return The chosen method (can be `N`, `FP`, `C`, `B`, or `SN).
  */
std::string ConfigFileReader::getMethod() const {

    auto algorithm = getValueByKey(ALGORITHM);

    // Convert the algorithm string to uppercase for case-insensitivity
    std::transform(algorithm.begin(), algorithm.end(), algorithm.begin(), ::toupper);

    // Check to see if algorithm can be mapped onto an actual method
    if (algorithm == "N" || algorithm == "FP" || algorithm == "C" || algorithm == "B" || algorithm == "SN") {
        return algorithm;
    } else {
        throw std::invalid_argument("Invalid algorithm: " + algorithm);
    }
}

    /**
     * @brief Getter for the Aitken bool.
     *
     * The function recognizes a number of different ways to set or not set the bool (1, true, yes and 0, false, no).
     * It is not case-sensitive, and throws an error for any other inputs/strings.
     *
     * @return True if Aitken acceleration is enabled, false otherwise.
     */
bool ConfigFileReader::getAitken() const {
    auto upperValue = getValueByKey(AITKEN);
    //auto upperValue = value;
    std::transform(upperValue.begin(), upperValue.end(), upperValue.begin(), ::toupper);

    if (upperValue == "0" || upperValue == "FALSE" || upperValue == "NO") {
        return false;
    } else if (upperValue == "1" || upperValue == "TRUE" || upperValue == "YES") {
        return true;
    } else {
        throw std::invalid_argument("Invalid boolean value: " + upperValue);
    }
}

/**
 * @brief Getter for the tolerance double.
 *
 * The tolerance value is turned into a double before being returned, and thus an error is automatically thrown
 * if this isn't possible due to the nature of the string.
 *
 * @return The tolerance value.
 */
double ConfigFileReader::getTolerance() const {
    return std::stod(getValueByKey(TOLERANCE));
}

/**
 * @brief Parses a string representing start points into a vector of doubles.
 *
 * The start points can be provided as a sequence of doubles separated by commas or spaces. This function a helper function
 * and is only accessed by getStartPoint(). If the format is invalid (unexpected characters, ... we throw an error).
 *
 * @param startPoints The string representing start points, seperated by commas or spaces.
 * @return A vector of doubles containing the parsed start points.
 */
std::vector<double> parseStartPointsPair(const std::string& startPoints) {
    std::istringstream iss(startPoints);
    char comma;
    double value;
    std::vector<double> result;

    while (iss >> value) {
        result.push_back(value);

        // Check for the comma
        if (!(iss >> comma) || (comma != ',' && comma != ' ' && comma != '\t')) {
            break;  // Break if not a comma or space
        }
    }

    if (!result.empty() && (iss.eof() || iss.fail())) {
        return result;
    }

    throw std::invalid_argument("Invalid start points format: " + startPoints);
}

    /**
     * @brief Getter for the startpoint (double)
     *
     * The start points can be a single double value (for `Newton` & `Fixed Point` methods) or a sequence of doubles separated by commas or spaces
     * (for `Chord` & `Bisection` methods = 2 values, can be more for `Newton` method for Systems depending on the amount of variables in the system).
     * This function parses the provided start points string and returns either a single double or a vector of doubles.
     *
     * @return A variant containing either a double or a vector of doubles representing the start point(s).
     */


    /**
      * @brief Getter for the startpoints (vector of doubles)
      *
      * The start points are a sequence of doubles separated by commas or spaces
      * (for `Chord` & `Bisection` methods = 2 values, can be more for `Newton` method for Systems depending on the amount of variables in the system).
      * This function parses the provided start points string and returns a vector of doubles.
      *
      */

std::vector<double> ConfigFileReader::getStartPoints() const {
    auto startPointsStr = getValueByKey(START_POINT);

    try {
        return parseStartPointsPair(startPointsStr);
    } catch (const std::invalid_argument&) {
        throw std::invalid_argument("Invalid start points format: " + startPointsStr);
    }
}


double ConfigFileReader::getStartPoint() const {
    auto startPointsStr = getValueByKey(START_POINT);

   try {
       return std::stod(startPointsStr);
   } catch (const std::invalid_argument&) {
       throw std::invalid_argument("Invalid start points format: " + startPointsStr);
   }

}

/**
 * @brief Getter for the iterations int.
 *
 * ìterations`resembles the maximal number of iterations that a method can do before it is returned, to avoid
 * it running eternally if a root can not be found. This number must be (able to be turned into) a positive integer.
 *
 * @return The maximum number of iterations.
 */
int ConfigFileReader::getIterations() const {
    auto iter = (int)std::ceil(std::stod(getValueByKey(MAX_ITERATIONS)));

    if (iter < 1) {
        throw std::invalid_argument("Invalid iterations " + std::to_string(iter));
    }
    return iter;
}

/**
* @brief Getter for multiple function equations, only accessible if method = Newton method for Systems
 *
 * An error is thrown if the value connected to the key `algorithm`is not "SN". Functions must be seperated by a "|",
*
* @return A vector containing the multiple function equations.
*/
std::vector<std::string> ConfigFileReader::getFunctions() const {

    if (getMethod() != "SN") {
        throw std::invalid_argument("This method " + getValueByKey(ALGORITHM) + " does not have multiple functions as an input\n");
    }

     auto input = getValueByKey(FUNCTION);
     std::istringstream iss(input);
     std::string function;
     std::vector<std::string> functions;

     while (std::getline(iss, function, '|')) {
         // Trim leading and trailing whitespaces from each element
         function.erase(0, function.find_first_not_of(" \t\r\n"));
         function.erase(function.find_last_not_of(" \t\r\n") + 1);

         functions.push_back(function);
     }

     return functions;
}

/**
  * @brief Getter for all variables, only accessible if method = Newton method for Systems
  *
  * An error is thrown if the value connected to the key `algorithm`is not "SN". Variables must be seperated by a "|",
  *
  * @return A vector containing the variables of the functions.
  */
std::vector<std::string> ConfigFileReader::getVariables() const {

    if (getMethod() != "SN") {
        throw std::invalid_argument("This method " + getValueByKey(ALGORITHM) + " does not have multiple variables as an input\n");
    }

    auto input = getValueByKey(VARIABLES);
    std::istringstream iss(input);
    std::string var;
    std::vector<std::string> vars;

    while (std::getline(iss, var, '|')) {
        // Trim leading and trailing whitespaces from each element
        var.erase(0, var.find_first_not_of(" \t\r\n"));
        var.erase(var.find_last_not_of(" \t\r\n") + 1);

        vars.push_back(var);
    }

    return vars;
}

/**
* @brief Getter for multiple derivatives, only accessible if method = Newton method for Systems
 *
 * An error is thrown if the value connected to the key `algorithm`is not "SN". Variables must be seperated by a "|",
*
* @return A vector containing the multiple derivatives.
*/
std::vector<std::vector<std::string>> ConfigFileReader::getDerivatives() const {

    if (getMethod() != "SN") {
        throw std::invalid_argument("This method " + getValueByKey(ALGORITHM) + " does not have multiple derivatives as an input\n");
    }

    auto input = getValueByKey(DERIVATIVE);
    std::istringstream iss(input);
    std::string der;
    std::vector<std::string> all_der;

    while (std::getline(iss, der, '|')) {
        // Trim leading and trailing whitespaces from each element
        der.erase(0, der.find_first_not_of(" \t\r\n"));
        der.erase(der.find_last_not_of(" \t\r\n") + 1);

        all_der.push_back(der);
    }

    int amount = (int)sqrt(all_der.size());
    std::vector<std::vector<std::string>> sorted_der;

    for (int i = 0; i < amount; i++) {

        std::vector<std::string> row_der(amount);

        for (int j = 0; j < amount; j++) {

            row_der[j] = all_der[i * amount + j];

        }
        sorted_der.push_back(row_der);
    }

    return sorted_der;
}


