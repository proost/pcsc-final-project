//
// Created by Anna  Proost on 05/12/2023.
//

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>


#include "exec/headers.hpp"
#include "inputs/headers.hpp"
#include "methods/headers.hpp"
#include "outputs/Solution.hpp"


int main(int argc, char** argv) {


    // ---------- 1. PROCESS THE INPUT ---------- //

    std::cout << "\n";

    if (argc < 3 || std::string(argv[1]) != "--config") {
        std::cerr << "\033[1;31mmain_exec should be launched as follows: " << argv[0] << " --config <config_file>\033[0m" << std::endl;
        return 1; // Return an error code
    }

    ConfigFileReader read_file(argv[2]);

    if (read_file.elements.empty()) {

        std::cerr << "\033[1;31mError opening file: " << argv[2] << "\033[0m" << std::endl;
        return 1; // Return an error code
    }

    // ------------------------------------------ //



    // ---------- 2. EXECUTE THE METHOD ---------- //


    Solution sol({0.0, 0.0}, false, MethodType::Newton);

    Method* p = Method::getMethod(read_file.getMethod(),false);
    if (read_file.getAitken()) p = Method::getMethod(read_file.getMethod(),true);

    Solver solver;
    if (read_file.getMethod() == "SN") {

        sol = p->operator()(solver.InputSystem(read_file),read_file.getStartPoints(),read_file.getTolerance(),read_file.getIterations());

    }

    else if ( read_file.getMethod() == "N" || read_file.getMethod() == "FP") {

        sol = p->operator()(solver.InputFunction(read_file),read_file.getStartPoint(),read_file.getTolerance(),read_file.getIterations());

    }

    else if ( read_file.getMethod() == "C" || read_file.getMethod() == "B") {

        sol = p->operator()(solver.InputFunction(read_file),read_file.getStartPoints(),read_file.getTolerance(),read_file.getIterations());

    }

    else {

        std::cerr << "\033[1;31mMethod is not recognized" << std::endl;
        return 1; // Return an error code

    }


    // ------------------------------------------ //




    // ----------- 3. PRINT THE OUTPUT ---------- //


    sol.printOutput(read_file);


    // ------------------------------------------ //

    return 0;

}

