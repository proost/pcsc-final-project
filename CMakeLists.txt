cmake_minimum_required(VERSION 3.26)
project(PCSC_final_project)

add_subdirectory(muparser)
add_subdirectory(googletest/googletest)
include_directories(eigen)

include_directories(eigen)

set(CMAKE_CXX_STANDARD 17)
#set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++ -static")


add_executable(test_suite
        tests.cpp

        inputs/Function.cpp
        inputs/MultiFunction.cpp
        inputs/MonoFunction.cpp
        inputs/System.cpp

        methods/Method.cpp
        methods/AccelerableMethod.cpp
        methods/Newton.cpp
        methods/FixedPoint.cpp
        methods/Chord.cpp
        methods/Bisection.cpp
        methods/Aitken.cpp

        outputs/Solution.cpp

        exec/ReadFile.cpp
        exec/Solver.cpp
)

add_executable(main_exec
        central_exec.cpp

        inputs/Function.cpp
        inputs/MultiFunction.cpp
        inputs/MonoFunction.cpp
        inputs/System.cpp

        methods/Method.cpp
        methods/AccelerableMethod.cpp
        methods/Newton.cpp
        methods/FixedPoint.cpp
        methods/Chord.cpp
        methods/Bisection.cpp
        methods/Aitken.cpp

        outputs/Solution.cpp

        exec/ReadFile.cpp
        exec/Solver.cpp

)
if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static-libgcc")
endif()

target_link_libraries(test_suite PRIVATE muparser gtest gtest_main)
target_link_libraries(main_exec PRIVATE muparser gtest gtest_main)
