#include <cmath>

#include "inputs/headers.hpp"
#include "methods/headers.hpp"
#include "exec/headers.hpp"

#include "gtest/gtest.h"


TEST(InputTest, MonoFunction) {

    MonoFunction test1("x^2 + exp(-x)", "-exp(-x)/x", "2*x - exp(-x)");
    EXPECT_EQ(std::pow(2,2) + exp(-2), test1(2.0));
    EXPECT_EQ(-exp(-2)/2, test1.fixed(2.0));
    EXPECT_EQ(4 -exp(-2), test1.der(2.0));

    EXPECT_EQ(std::pow(-3,2) + exp(3), test1(-3.0));
    EXPECT_EQ(exp(3)/3, test1.fixed(-3.0));
    EXPECT_EQ(-6 - exp(3), test1.der(-3.0));

}

TEST(InputTest, MultiFunction) {

    MultiFunction test1("x^2 + exp(-y) + y*x", {"2*x + y", "-exp(-y) + x"}, {"x", "y"});
    Eigen::VectorXd input(2);

    input << 2.0, -1.0;
    EXPECT_EQ(std::pow(2,2) + exp(1) - 2, test1(input));
    EXPECT_EQ(3, test1.der(input)[0]);
    EXPECT_EQ(-exp(1) + 2, test1.der(input)[1]);

    input << -3.0, 2.5;

    EXPECT_EQ(std::pow(-3,2) + exp(-2.5) - 2.5*3, test1(input));
    EXPECT_EQ(-3.5, test1.der(input)[0]);
    EXPECT_EQ(-exp(-2.5) -3, test1.der(input)[1]);

}

TEST(InputTest, System) {

    MultiFunction m1("x^2 + exp(-y) + y*x", {"2*x + y", "-exp(-y) + x"}, {"x", "y"});
    MultiFunction m2("x^3 - 2*y*x", {"3*x^2 - 2*y", "-2*x"}, {"x", "y"});
    System test1({m1, m2});

    Eigen::VectorXd input(2);

    input << 2.0, -1.0;
    EXPECT_EQ(std::pow(2,2) + exp(1) - 2, test1(input)[0]);
    EXPECT_EQ(std::pow(2,3) + 2*2, test1(input)[1]);

    EXPECT_EQ(3, test1.getJacobian(input)(0,0));
    EXPECT_EQ(-exp(1) + 2, test1.getJacobian(input)(0,1));
    EXPECT_EQ(3*std::pow(2,2) +2, test1.getJacobian(input)(1,0));
    EXPECT_EQ(-4, test1.getJacobian(input)(1,1));

    input << -3.0, 2.5;
    EXPECT_EQ(std::pow(-3,2) + exp(-2.5) - 3*2.5, test1(input)[0]);
    EXPECT_EQ(std::pow(-3,3) + 3*2*2.5, test1(input)[1]);

    EXPECT_EQ(-3.5, test1.getJacobian(input)(0,0));
    EXPECT_EQ(-exp(-2.5) -3, test1.getJacobian(input)(0,1));
    EXPECT_EQ(3 * std::pow(-3,2) - 2*2.5, test1.getJacobian(input)(1,0));
    EXPECT_EQ(6, test1.getJacobian(input)(1,1));

}


TEST(NewtonTest, Converging) {

    Solver solver;

    Solution test1 = solver.NewtonMethod("x^2 - 612", "2 * x", 1.0, 1e-10, 1000);
    EXPECT_TRUE(test1.hasConverged());
    ASSERT_NEAR(24.73863375370596329892846, test1.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x^2 - 612", test1.getRoot()), 1e-10);

    Solution test1a = solver.NewtonMethodA("x^2 - 612", "2 * x", 1.0, 1e-10, 1000);
    EXPECT_TRUE(test1a.hasConverged());
    ASSERT_NEAR(24.73863375370596329892846, test1a.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x^2 - 612", test1a.getRoot()), 1e-10);

    EXPECT_TRUE(test1a.getNIterations() <= test1.getNIterations());

    Solution test2 = solver.NewtonMethod("cos(x) - x^3", "-sin(x) - 3 * x^2", 0.5, 1e-15, 1000);
    EXPECT_TRUE(test2.hasConverged());
    ASSERT_NEAR(0.8654740331016145, test2.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("cos(x) - x^3", test2.getRoot()), 1e-10);

    Solution test2a = solver.NewtonMethodA("cos(x) - x^3", "-sin(x) - 3 * x^2", 0.5, 1e-15, 1000);
    EXPECT_TRUE(test2a.hasConverged());
    ASSERT_NEAR(0.8654740331016145, test2a.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("cos(x) - x^3", test2a.getRoot()), 1e-10);

    EXPECT_TRUE(test2a.getNIterations() <= test2.getNIterations());

    Solution test3 = solver.NewtonMethod("x^2 - 2", "2 * x", 1.0, 1e-10, 100);
    EXPECT_TRUE(test3.hasConverged());
    ASSERT_NEAR(1.414213562373095048801688724209698078569, test3.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x^2 - 2", test3.getRoot()), 1e-10);

    Solution test3a = solver.NewtonMethodA("x^2 - 2", "2 * x", 1.0, 1e-10, 100);
    EXPECT_TRUE(test3a.hasConverged());
    ASSERT_NEAR(1.414213562373095048801688724209698078569, test3a.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x^2 - 2", test3a.getRoot()), 1e-10);

    EXPECT_TRUE(test3a.getNIterations() <= test3.getNIterations());

    Solution test4 = solver.NewtonMethod("x/(1+x^2)", "(1-x^2)/((1+x^2)^2)", 0.1, 1e-10, 100);
    EXPECT_TRUE(test4.hasConverged());
    ASSERT_NEAR(0, test4.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x/(1+x^2)", test4.getRoot()), 1e-10);

    Solution test4a = solver.NewtonMethod("x/(1+x^2)", "(1-x^2)/((1+x^2)^2)", 0.1, 1e-10, 100);
    EXPECT_TRUE(test4.hasConverged());
    ASSERT_NEAR(0, test4.getRoot(), 1e-18);
    ASSERT_NEAR(0, solver.EvaluateFunction("x/(1+x^2)", test4.getRoot()), 1e-10);

    EXPECT_TRUE(test4a.getNIterations() <= test4.getNIterations());

}

TEST(NewtonTest, Diverging) {

    Solver solver;

    Solution test1 = solver.NewtonMethod("x^(1/3)", "(1/3) * x^(-2/3)", 0.1, 1e-2, 100);
    EXPECT_FALSE(test1.hasConverged());

    Solution test2 = solver.NewtonMethod("x/(1+x^2)", "(1-x^2)/((1+x^2)^2)", 2, 1e-10, 20);
    EXPECT_FALSE(test2.hasConverged());

}

TEST(NewtonTest, System) {

    Solver solver;

    Solution test1 = solver.SystemNewtonMethod({"x^2 + y^2 - 10", "x*y-5"}, {{"2*x", "2*y"}, {"y", "x"}},{"x", "y"},  {2.1, 2.0}, 1e-10, 100);
    EXPECT_TRUE(test1.hasConverged());

    ASSERT_NEAR(0, solver.EvaluateMultiFunction("x^2 + y^2 - 10", test1.getVectorRoot(), {"x", "y"}), 1e-10);
    ASSERT_NEAR(0, solver.EvaluateMultiFunction("x*y-5", test1.getVectorRoot(), {"x", "y"}), 1e-10);


    // if start points are equal, there is automatically a small perturbation added to one of them to avoid an in-invertible jacobian.
    Solution test2 = solver.SystemNewtonMethod({"x^2 + y^2 - 10", "x*y-5"}, {{"2*x", "2*y"}, {"y", "x"}},{"x", "y"},  {2.0, 2.0}, 1e-10, 100);
    EXPECT_TRUE(test2.hasConverged());

    ASSERT_NEAR(0, solver.EvaluateMultiFunction("x^2 + y^2 - 10", test2.getVectorRoot(), {"x", "y"}), 1e-10);
    ASSERT_NEAR(0, solver.EvaluateMultiFunction("x*y-5", test2.getVectorRoot(), {"x", "y"}), 1e-10);



    Solution test3 = solver.SystemNewtonMethod({"x^2 + y^2 - 1", "exp(x*y) - sin(y)"}, {{"2*x", "2*y"}, {"y * exp(x*y)", "x * exp(x*y) - cos(y)"}},{"x", "y"},  {0.7, 0.6}, 1e-10, 200);
    EXPECT_TRUE(test3.hasConverged());

    ASSERT_NEAR(0, solver.EvaluateMultiFunction("x^2 + y^2 - 1", test3.getVectorRoot(), {"x", "y"}), 1e-10);
    ASSERT_NEAR(0, solver.EvaluateMultiFunction("exp(x*y) - sin(y)", test3.getVectorRoot(), {"x", "y"}), 1e-10);

}

TEST(FixedPointTest, Converging) {

    Solver solver;

    Solution test1 = solver.FixedPointMethod("x^4 - x - 10", "(x+10)^(1/4)", 1.0, 1e-10, 100);
    EXPECT_TRUE(test1.hasConverged());
    ASSERT_NEAR(1.855584528640965, test1.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x^4 - x - 10", test1.getRoot()), 1e-10);

    Solution test1a = solver.FixedPointMethodA("x^4 - x - 10", "(x+10)^(1/4)", 1.0, 1e-10, 100);
    EXPECT_TRUE(test1a.hasConverged());
    ASSERT_NEAR(1.855584528640965, test1a.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x^4 - x - 10", test1a.getRoot()), 1e-10);

    EXPECT_TRUE(test1a.getNIterations() <= test1.getNIterations());

    Solution test2 = solver.FixedPointMethod("cos(x) - (x * exp(x))", "cos(x)/exp(x)", 2.0, 1e-13, 150);
    EXPECT_TRUE(test2.hasConverged());
    ASSERT_NEAR(0.51775736386568516068, test2.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("cos(x) - (x * exp(x))", test2.getRoot()), 1e-13);

    Solution test2a = solver.FixedPointMethodA("cos(x) - (x * exp(x))", "cos(x)/exp(x)", 2.0, 1e-13, 150);

    EXPECT_TRUE(test2a.hasConverged());
    ASSERT_NEAR(0.51775736386568516068, test2a.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("cos(x) - (x * exp(x))", test2a.getRoot()), 1e-13);



    EXPECT_TRUE(test2a.getNIterations() <= test2.getNIterations());

    Solution test3 = solver.FixedPointMethod("x - sin(x) - (1/2)", "sin(x) + (1/2)", 2.0, 1e-10, 100);
    EXPECT_TRUE(test3.hasConverged());
    ASSERT_NEAR(1.497300389095987, test3.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x - sin(x) - (1/2)", test3.getRoot()), 1e-10);

    Solution test3a = solver.FixedPointMethodA("x - sin(x) - (1/2)", "sin(x) + (1/2)", 2.0, 1e-10, 100);
    EXPECT_TRUE(test3a.hasConverged());
    ASSERT_NEAR(1.497300389095987, test3a.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x - sin(x) - (1/2)", test3a.getRoot()), 1e-10);

    EXPECT_TRUE(test3a.getNIterations() <= test3.getNIterations());

}

TEST(FixedPointTest, Diverging) {

    Solver solver;

    Solution test1 = solver.FixedPointMethod("x^4 - x - 10", "10/(x^3-1)", 2.0, 1e-15, 50);
    EXPECT_FALSE(test1.hasConverged());

    Solution test2 = solver.FixedPointMethod("x^4 - x - 10", "sqrt(x+10)/x", 1.8, 1e-5, 100);
    EXPECT_FALSE(test2.hasConverged());

}

TEST(ChordTest, Converging) {

    Solver solver;

    Solution test1 = solver.ChordMethod("x^2 - 612", {100.0, 30.0}, 1e-10, 100);
    EXPECT_TRUE(test1.hasConverged());
    ASSERT_NEAR(24.73863375370596329892846, test1.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x^2 - 612", test1.getRoot()), 1e-10);

    Solution test2 = solver.ChordMethod("x^3 - x - 1", {1.0, 2.0}, 1e-10, 200);
    EXPECT_TRUE(test2.hasConverged());
    ASSERT_NEAR(1.324717957, test2.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x^3 - x - 1", test2.getRoot()), 1e-10);

    Solution test3 = solver.ChordMethod("exp(-x) - x", {0.0, 1.0}, 1e-10, 200);
    EXPECT_TRUE(test3.hasConverged());
    ASSERT_NEAR(0.567143289944068, test3.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("exp(-x) - x", test3.getRoot()), 1e-10);

}

TEST(ChordTest, Diverging) {

    Solver solver;

    Solution test1 = solver.ChordMethod("log(x)", {0.5, 5.0}, 1e-10, 50);
    EXPECT_FALSE(test1.hasConverged());

    Solution test2 = solver.ChordMethod("tan(x)", {M_PI/2 -0.1, M_PI/2 + 0.1}, 1e-10, 200);
    EXPECT_FALSE(test2.hasConverged());

}

TEST(BisectionTest, Converging) {

    Solver solver;

    Solution test1 = solver.BisectionMethod("x^3 - x - 2", {1.0, 2.0}, 1e-10, 100);
    EXPECT_TRUE(test1.hasConverged());
    ASSERT_NEAR(1.521379707218907, test1.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x^3 - x - 2", test1.getRoot()), 1e-9);

    Solution test2 = solver.BisectionMethod("10 - x^2", {-2.0, 5.0}, 1e-10, 200);
    EXPECT_TRUE(test2.hasConverged());
    ASSERT_NEAR(3.162277660168389, test2.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("10 - x^2", test2.getRoot()), 1e-9);

    Solution test3 = solver.BisectionMethod("x - exp(-x)", {0.55, 0.6}, 1e-10, 200);
    EXPECT_TRUE(test3.hasConverged());
    ASSERT_NEAR(0.567143289944068, test3.getRoot(), 1e-8);
    ASSERT_NEAR(0, solver.EvaluateFunction("x - exp(-x)", test3.getRoot()), 1e-9);

}

TEST(BisectionTest, Diverging) {

    Solver solver;

    Solution test1 = solver.BisectionMethod("x - exp(-x)", {-5.0, 0}, 1e-10, 50);
    EXPECT_FALSE(test1.hasConverged());

    Solution test2 = solver.BisectionMethod("10 - x^2", {-2.0, 3.0}, 1e-10, 200);
    EXPECT_FALSE(test2.hasConverged());

}

TEST(ConfigFile, Inputs) {

    ConfigFileReader read_test("../input_config/input1.txt");

    /*
     * # General settings
     * function = x^2 + y^2 - 10 | x*y-5
     * variables = x | y
     * fixed_point =
     * derivative = 2*x | 2*y | y | x
     *
     * # Method settings
     * algorithm = SN
     * aitken = no
     * tolerance = 1e-10
     * max_iterations = 1e3
     * start_point = 2.1 , 2.0
     */

    EXPECT_EQ(read_test.getIterations(), 1e3);
    EXPECT_EQ(read_test.getTolerance(), 1e-10);
    EXPECT_EQ(read_test.getMethod(), "SN");

    EXPECT_EQ(read_test.getVariables()[1], "y");
    EXPECT_EQ(read_test.getFunctions()[0], "x^2 + y^2 - 10");

    EXPECT_EQ(read_test.getDerivatives()[0][1], "2*y");
    EXPECT_EQ(read_test.getDerivatives()[1][0], "y");

    EXPECT_EQ(read_test.getStartPoints()[0], 2.1);
    EXPECT_EQ(read_test.getStartPoints()[1], 2.0);
}

TEST(ConfigFile, Errors) {

    ConfigFileReader read_test("../input_config/input2.txt");

    /*
     * # General settings
     * function = x^2 - 612
     * variables =
     * fixed_point =
     * derivative =
     *
     * # Method settings
     * algorithm = C
     * aitken = no
     * tolerance = 1e-10
     * max_iterations = 1e3
     * start_point = 100.0 , 30.0
     */

    EXPECT_EQ(read_test.getIterations(), 1e3);
    EXPECT_EQ(read_test.getTolerance(), 1e-10);
    EXPECT_EQ(read_test.getMethod(), "C");

    EXPECT_EQ(read_test.getStartPoints()[0], 100.0);
    EXPECT_EQ(read_test.getStartPoints()[1], 30.0);

    EXPECT_THROW(read_test.getFunctions(), std::invalid_argument);
    EXPECT_THROW(read_test.getVariables(), std::invalid_argument);
    EXPECT_THROW(read_test.getDerivatives(), std::invalid_argument);

    EXPECT_THROW(read_test.getDerivative(), std::invalid_argument);
    EXPECT_THROW(read_test.getFixedPoint(), std::invalid_argument);
}


int main() {

    testing::InitGoogleTest();
    return RUN_ALL_TESTS();

}