//
// Created by Gal on 20.11.2023.
//

#ifndef PCSC_FINAL_PROJECT_SOLUTION_HPP
#define PCSC_FINAL_PROJECT_SOLUTION_HPP
#include <cmath>
#include <vector>
#include <string>
#include "../methods/MethodType.hpp"
#include "../exec/ReadFile.hpp"
#include "Eigen/Core"

/**
 * @brief The Solution class stores the results of numerical methods, including iterations and convergence status.
 *
 * It supports storing scalar, interval, and vector iterations and provides functionality to access these results
 * and check for convergence.
 */
class Solution {
public:
     /**
     * @brief Constructs a Solution object for scalar iteration methods.
     */
    explicit Solution(std::vector<double> iterations,bool converged,MethodType type);
        /**
     * @brief Constructs a Solution object for interval iteration methods.
     */
    explicit Solution(std::vector<std::pair<double,double>> iterations,bool converged,MethodType type);
        /**
     * @brief Constructs a Solution object for vector iteration methods.
     */
    explicit Solution(std::vector<Eigen::VectorXd> iterations, bool converged, MethodType type);
        /**
     * @brief Checks if the numerical method converged.
     */
    bool hasConverged() const;
    /**
     * @brief Gets the number of iterations performed.
     */
    std::vector<double> getIterations() const;
        /**
     * @brief Gets the number of iterations performed.
     */
    int getNIterations() const;
        /**
     * @brief Gets the interval iterations as a vector of pairs of doubles.
     */
    std::vector<std::pair<double,double>> getIntervalIterations() const;
        /**
     * @brief Gets the root calculated by the numerical method.
     */
    double getRoot() const;
        /**
     * @brief Gets the root (or solution) for vector-based methods.
     */
    Eigen::VectorXd getVectorRoot() const;

    /**
      * @brief Calls for `Solution` to print the output based on the chosen method and configuration.
      */
    void printOutput(ConfigFileReader file);

private:
    /**
     * @brief Prints an Eigen::VectorXd to the standard output.
     */
    static void printVector(const Eigen::VectorXd& vec);

    /**
      * @brief Converts an Eigen::VectorXd to a std::string
      */
    std::string stringVector(const Eigen::VectorXd& vec);

    std::vector<double> iterations;///< Vector of doubles for scalar iterations.
    std::vector<std::pair<double,double>> interval_iterations;///< Vector of Eigen::VectorXd for vector iterations.
    std::vector<Eigen::VectorXd> vector_iterations;///< Vector of Eigen::VectorXd for vector iterations.
    unsigned int iter_length;///< The length of the iterations.
    bool converged;///< Boolean indicating if the method converged.
    MethodType type;///< The type of numerical method used.
};


#endif //PCSC_FINAL_PROJECT_SOLUTION_HPP
