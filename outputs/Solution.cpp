//
// Created by Gal on 20.11.2023.
//

// This Solution class contains the sequence of iterations and is used by different numerical methods

#include "Solution.hpp"
#include <cmath>
#include "../methods/MethodType.hpp"
#include "../exec/ReadFile.hpp"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <utility>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>


/**
 * @brief Constructs a new Solution object for methods resulting in scalar iterations.
 * @param iterations A vector of doubles representing the iterations.
 * @param converged A boolean indicating whether the method converged.
 * @param type The type of numerical method used (MethodType enumeration).
 */
Solution::Solution(std::vector<double> iterations,bool converged,MethodType type) : // Newton Chord FixedPoint
converged(converged),iterations(iterations),type(type){
    iter_length=iterations.size();
}
/**
 * @brief Constructs a new Solution object for methods resulting in interval iterations (e.g., Bisection).
 * @param iter A vector of pairs of doubles representing the interval iterations.
 * @param converged A boolean indicating whether the method converged.
 * @param type The type of numerical method used (MethodType enumeration).
 */
Solution::Solution(std::vector<std::pair<double,double>> iter,bool converged,MethodType type) : // Bisection
converged(converged),interval_iterations(std::move(iter)),type(type){
    iter_length = interval_iterations.size();
}
/**
 * @brief Constructs a new Solution object for methods resulting in vector iterations (e.g., multi-dimensional Newton).
 * @param iterations A vector of Eigen::VectorXd representing the iterations.
 * @param converged A boolean indicating whether the method converged.
 * @param type The type of numerical method used (MethodType enumeration).
 */
Solution::Solution(std::vector<Eigen::VectorXd> iterations, bool converged, MethodType type) : // Newton multi dimensional
converged(converged), vector_iterations(iterations) ,type(type){
    iter_length = vector_iterations.size();

}
/**
 * @brief Checks if the numerical method converged.
 * @return true if the method converged, false otherwise.
 */
bool Solution::hasConverged() const {
    return converged;
}
/**
 * @brief Gets the iterations as a vector of doubles.
 * @return A vector of doubles representing the iterations.
 */
std::vector<double> Solution::getIterations() const {
    return iterations;
}
/**
 * @brief Gets the number of iterations performed.
 * @return The number of iterations as an integer.
 */
int Solution::getNIterations() const {
    if(type==MethodType::Chord) {return (int)iter_length - 2;}
    return (int)iter_length - 1;
}
/**
 * @brief Gets the interval iterations as a vector of pairs of doubles.
 * @return A vector of pairs of doubles representing the interval iterations.
 */
std::vector<std::pair<double,double>> Solution::getIntervalIterations() const {

    return interval_iterations;
}
/**
 * @brief Gets the root calculated by the numerical method.
 * @return The root as a double. In case of interval-based methods, it returns the midpoint of the last interval.
 */
double Solution::getRoot() const {
    if(type == MethodType::Bisection)
        return (interval_iterations.back().first + interval_iterations.back().second)/2;
    return iterations.back();
}
/**
 * @brief Gets the root (or solution) for vector-based methods.
 * @return An Eigen::VectorXd representing the root.
 */
Eigen::VectorXd Solution::getVectorRoot() const{
    return vector_iterations.back();
}

/**
 * @brief Prints an Eigen::VectorXd to the standard output.
 * @param vec The Eigen::VectorXd to be printed.
 */
void Solution::printVector(const Eigen::VectorXd& vec) {
    std::cout << "("<<std::fixed << std::setprecision(15);
    for (int i = 0; i < vec.size(); ++i) {
        std::cout << vec[i];
        if (i < vec.size() - 1) {
            std::cout << ", ";
        }
    }
    std::cout << ")" << std::endl;
}


/**
 * @brief Converts an Eigen::VectorXd to a std::string
 *
 * @param vec The Eigen::VectorXd to be converted.
 * @return a std::string that represents the same iterations.
 */
std::string Solution::stringVector(const Eigen::VectorXd& vec) {
    std::ostringstream oss;
    oss << "(" << std::fixed << std::setprecision(15);
    for (int i = 0; i < vec.size(); ++i) {
        oss << vec[i];
        if (i < vec.size() - 1) {
            oss << ", ";
        }
    }
    oss << ")";
    return oss.str();
}

    /**
      * @brief Prints all info about the solution in a clean & consistent manner
      *
      * This function outputs the results of our method to find the root or roots of a function.
      * It prints the final result, if it convergences or not, initial parameters, and iterative steps, depending on
      * the method type. The output is formatted for clarity and precision.
      *
      * Supported method types are Newton, Fixed Point, Bisection, Chord and NewtonMulti. Some methods can also handle
      * Aitken acceleration if applicable & it is printed whether this was used or not.
      * The output includes whether the method converged, the number of iterations,
      * initial points or intervals, and the value at each iteration. For systems of equations (NewtonMulti),
      * it prints the vector of solutions.
      *
      * @param file A ConfigFileReader object that contains configuration parameters for the methods,
      * such as the function(s) to be solved, the method type, and other relevant settings.
      */
void Solution::printOutput(ConfigFileReader file) {

    std::string combinedFunctions;
    std::string vectorRoot;

    switch(type){

        case(MethodType::Bisection):

            if (hasConverged()) std::cout << "The root \033[1m"<< std::fixed << std::setprecision(15) << getRoot() << "\033[0m of the function f(x) = " << file.getFunction() << " in interval [" << getIntervalIterations()[getNIterations()].first << ", " << getIntervalIterations()[getNIterations()].second << "] was found by the " << file.getMethod() << " method in " <<  getNIterations() <<" iterations. \n";
            else std::cout << "The " << file.getMethod() << " method does not converge for the initial point or within the max. number of iterations. \n";

            std::cout << "\nThis is an overview of the results:"<<std::endl;

            std::cout<<"\n------------------------------ " << std::endl;

            if(hasConverged())
                std::cout<<"Converged : \033[1mYES\033[0m" << std::endl;
            else
                std::cout<<"Converged : \033[1mNO\033[0m" << std::endl;

            std::cout << "Start interval " << ": [" << std::fixed << std::setprecision(15) << interval_iterations[0].first<< ", "<<interval_iterations[0].second<<"]"<<std::endl;

            for(int i=1;i<iter_length;i++){
                std::cout<<"Iteration "<<i<<" : [" << std::fixed << std::setprecision(15) << interval_iterations[i].first<< ", "<<interval_iterations[i].second<<"]"<<std::endl;
            }
            break;

        case(MethodType::Chord):

            if (hasConverged()) std::cout << "The root \033[1m"<< std::fixed << std::setprecision(15) << getRoot() << "\033[0m of the function f(x) = " << file.getFunction() <<" was found by the " << file.getMethod() << " method in " <<  getNIterations() <<" iterations. \n";
            else std::cout << "The " << file.getMethod() << " method does not converge for the initial point or within the max. number of iterations. \n";

            std::cout << "\nThis is an overview of the results:"<<std::endl;

            std::cout<<"\n------------------------------ " << std::endl;

            std::cout<<"Iterations : "<<iter_length - 2<<std::endl;

            std::cout << "Start point 1" << ": " << std::fixed << std::setprecision(15) << iterations[0] << std::endl;
            std::cout << "Start point 2" << ": " << std::fixed << std::setprecision(15) << iterations[1] << std::endl;

            for(int i=2;i<iter_length;i++){

                std::cout<<"Iteration "<<i-1<<" : " << std::fixed << std::setprecision(15) << iterations[i]<<std::endl;
            }
            break;

        case(MethodType::NewtonMulti):

            combinedFunctions = std::accumulate(
                    file.getFunctions().begin() + 1, file.getFunctions().end(),
                    file.getFunctions()[0],
                    [](const std::string& acc, const std::string& str) {
                        return acc + ", " + str;
                    }
            );

            vectorRoot = stringVector(vector_iterations.back());

            if (hasConverged()) {
                std::cout << "The root\033[1m " << std::fixed << std::setprecision(15) << vectorRoot << "\033[0m of the functions {" << combinedFunctions <<
                          "} was found by the Newton method for Systems in "
                          << getNIterations() << " iterations. \n";
            }

            else std::cout << "The Newton method for Systems does not converge for the initial points or within the max. number of iterations. \n";

            std::cout << "\nThis is an overview of the results:"<<std::endl;

            std::cout<<"Iterations : "<<iter_length - 1<<std::endl;

            std::cout<<"\n------------------------------ " << std::endl;

            if(hasConverged())
                std::cout<<"Converged : \033[1mYES\033[0m" << std::endl;
            else
                std::cout<<"Converged : \033[1mNO\033[0m" << std::endl;

            std::cout << "Start points : ";
            printVector(vector_iterations[0]);
            for(int i=1;i<iter_length;i++){
                std::cout << "Iteration " << i << ": " ;
                printVector(vector_iterations[i]);
            }
            break;

        default:

            if (file.getAitken()) std::cout << "\n\033[1mMethod was run with Aitken acceleration\033[0m\n";

            if (hasConverged()) std::cout << "The root \033[1m"<< std::fixed << std::setprecision(15) << getRoot() << "\033[0m of the function f(x) = " << file.getFunction() <<" was found by the " << file.getMethod() << " method in " <<  getNIterations() <<" iterations. \n";
            else std::cout << "The " << file.getMethod() << " method does not converge for the initial point or within the max. number of iterations. \n";

            std::cout << "\nThis is an overview of the results:"<<std::endl;

            std::cout<<"\n------------------------------ " << std::endl;

            std::cout<<"Iterations : "<<iter_length - 1<<std::endl;

            if(hasConverged())
                std::cout<<"Converged : \033[1mYES\033[0m" << std::endl;
            else
                std::cout<<"Converged : \033[1mNO\033[0m" << std::endl;

            std::cout << "Start point " << ": " << std::fixed << std::setprecision(15) << iterations[0] << std::endl;
            for (int i = 1; i < iter_length; i++) {
                std::cout << "Iteration " << i << ": " << std::fixed << std::setprecision(15) << iterations[i] << std::endl;
            }
    }
    std::cout<<"------------------------------ \n" << std::endl;

}