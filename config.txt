# General settings
function = x^2 - 612
variables =
fixed_point =
derivative = 2*x

# Method settings
algorithm = N
aitken = no
tolerance = 1e-10
max_iterations = 1e3
start_point = 10.0