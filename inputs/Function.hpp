#ifndef PCSC_FINAL_PROJECT_FUNCTION_HPP
#define PCSC_FINAL_PROJECT_FUNCTION_HPP

#include <utility>
#include <string>
#include "muParser.h"

/**
 * @brief The Function Class serves as a base class for MonoFunction and MultiFunction, providing
 * an abstraction for handling equations & parsers.
 *
 * It is not possible to directly define an instance of the Function class. The constructor
 * is only used via subclasses to provide polymorphism in setting the equation & parser.
 *
 * The getters getEquation() & getAmountVars() can be used for instances of both
 * subclasses and return the relevant fields.
 */
class Function{

protected:

    /**
     *  The Function class stores/defines the following elements:
     *
     *  @var equation - the actual equation in string form.
     *  @var eq_parser - a parser of the equation that can be used to evaluate it for (a) given value(s)
     *  @var amount_vars - the amount of variables of the equation (1 for MonoFunction, >1 for Multifunction)
     *
     */
    std::string equation;
    mu::Parser eq_parser;

    int amount_vars;


    /**
     * @brief Constructor for the Function class, can only be accessed by instances of its daughter classes (MonoFunction & MultiFunction)
     *
     * Initializes a new instance of the Function class, linked to the subclass that called it.
     */
    Function(const std::string& eq, int amount_vars);

public:

    /**
     * @brief Getter for the equation.
     */
    std::string getEquation() const;

    /**
     * @brief Getter for the amount of variables.
     */
    int getAmountVars() const;

};

#endif //PCSC_FINAL_PROJECT_FUNCTION_HPP
