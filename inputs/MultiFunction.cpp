#include "MultiFunction.hpp"
#include <muParser.h>


    /**
    * @brief Constructor for the MultiFunction class.
    *
    * Initializes a multivariable function with its main equation and its derivatives. it also saves the variables
    * of the function & the order all values will be evaluated in/mapped to from then on.
    * The parsers for the function & derivatives are also created, either within the Function() constructor call
    * or separately in the MultiFunction() constructor for the derivatives.
    *
    * In the call to the Function() constructor, we automatically set `amount_vars`to var.size().
    *
    * @param eq The main equation in string form.
    * @param der A vector with all partial derivatives (first the one of the first element in var, then of the second, ...)
    * @param var A vector with the different variables in string form. The order in this vector is crucial and must match the order of values
    *  when evaluating the function/its derivatives (e.g., the first value maps to `x`, the second to `y`, ...), in order to work properly.
    */
MultiFunction::MultiFunction(std::string eq, std::vector<std::string> der, std::vector<std::string> var)
    : Function(eq, (int)var.size()) {

    if (der.size() != var.size()) {
        std::cerr << "Error: There was not a 1-on-1 input for variables & the derivatives" << std::endl;
        return;
    }

    derivative = der;
    variables = var;

    der_parser.resize(var.size());

    for (std::size_t i = 0; i < var.size(); ++i) {
        der_parser[i].SetExpr(der[i]);
    }

}

    /**
     * @brief Getter for the (partial) derivatives.
     *
     * This method can be used by instances of MultiFunction
     * and returns all partial derivatives as std::strings.
     *
     * @return A vector with all (partial) derivatives in string form.
     */
std::vector<std::string> MultiFunction::getDerivative() const { return derivative; }

/**
 * @brief Overloaded function call operator.
 *
 * Evaluates the equation for the given values of the variables, using eq_parser.
 *
 * @param values The input values (Eigenvector of values).
 * @return The result of evaluating the equation in the `values` (1 double).
 */
double MultiFunction::operator()(Eigen::VectorXd values) {

    if (values.size() != amount_vars) {
        std::cerr << "Error: There were not the right amount of variables/values to evaluate the expression" << std::endl;
        return 0;
    }

    for (int i = 0; i < amount_vars; ++i) { eq_parser.DefineVar(variables[i], &values[i]); }

    return eq_parser.Eval();
}

/**
* @brief Computes the (partial) derivatives for a given input `values` of the variables.
*
* Evaluates the derivatives for the given values of the variables, using der_parser.
*
* @param values The input values (Eigenvector of values).
* @return The result of evaluating the derivative in `x` (returned as an Eigenvector).
*
* Once again just like with everything else, the order of elements in the returned Eigenvector corresponds to the order of variables.
*/
Eigen::VectorXd MultiFunction::der(Eigen::VectorXd values) {

    if (values.size() != amount_vars) {
        std::cerr << "Error: There were not the right amount of variables/values to evaluate the expression" << std::endl;
        return Eigen::VectorXd::Zero(amount_vars);
    }

    Eigen::VectorXd derivatives(amount_vars);
    auto fill_parsers = der_parser;

    for (int i = 0; i < amount_vars; ++i) {

        for (int j = 0; j < amount_vars; ++j) {

            fill_parsers[i].DefineVar(variables[j], &values[j]);
        }

        derivatives[i] = fill_parsers[i].Eval();
    }

    return derivatives;
}


