//
// Created by Gal on 08.12.2023.
//

#ifndef PCSC_FINAL_PROJECT_HEADERS_HPP
#define PCSC_FINAL_PROJECT_HEADERS_HPP

#include "Function.hpp"
#include "MonoFunction.hpp"
#include "MultiFunction.hpp"
#include "System.hpp"
#endif //PCSC_FINAL_PROJECT_HEADERS_HPP
