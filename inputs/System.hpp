#ifndef PCSC_FINAL_PROJECT_SYSTEM_HPP
#define PCSC_FINAL_PROJECT_SYSTEM_HPP

#include <vector>
#include "muParser.h"
#include "MultiFunction.hpp"
#include <Eigen/Dense>

/**
 * @brief Represents a System of MultiFunctions (in multiple variables e.g. `x, y, z ...`).
 * It is basically a vector of MultiFunctions on which we can perform the Newton Method for Systems.
 *
 * There are a number of getters, both to get the relevant equations of all the MultiFunctions in the System, as well
 * as to get the equations of a specific MultiFunction only (given the corresponding index as input)
 *
 * To evaluate the derivatives for certain variables values, we call the getJacobian() function, which returns an Eigen::Matrix with the values.
 * we overload the () operator with the values we want the functions to be evaluated in, and get a vector with all the evaluations in return.
 *
 * @code
 * // We define 2 instances of the MultiFunction class
 * std::string mainEquation1 = "x^2 - x*y";
 * std::vector<std::string> derivativeEquations1 = {"2 * x - y", "x"};
 * std::vector<std::string> Variables1 = {"x", "y"};
 *
 * std::string mainEquation2 = "x + y";
 * std::vector<std::string> derivativeEquations2 = {"1", "1"};
 * std::vector<std::string> Variables2 = {"x", "y"};
 *
 * MultiFunction multiFunc1(mainEquation1, derivativeEquations1, Variables1);
 * MultiFunction multiFunc2(mainEquation2, derivativeEquations2, Variables2);
 *
 * System syst({multiFunc1, multiFunc2});
 *
 * // Example: Evaluate the System for x = 3 and y = -1
 * Eigen::VectorXd inputValues(2);
 * inputValues << 3.0, -1.0;
 *
 * Eigen::VectorXd result = syst(inputValues);
 * Eigen::MatrixXd result_der = syst.getJacobian(inputValues);
 *
 * std::cout << "Result of evaluating the first function at x = 3, y = -1: " << result[0] << std::endl;
 * std::cout << "Result of evaluating the second function at x = 3, y = -1: " << result[1] << std::endl;
 *
 * std::cout << "Result of evaluating the derivative of x of the first function at x = 3, y = -1: " << result_der[0][0] << std::endl;
 * std::cout << "Result of evaluating the derivative of y of the first function at x = 3, y = -1: " << result_der[0][1] << std::endl;
 *
 * std::cout << "Result of evaluating the derivative of x of the second function at x = 3, y = -1: " << result_der[1][0] << std::endl;
 * std::cout << "Result of evaluating the derivative of y of the second function at x = 3, y = -1: " << result_der[1][1] << std::endl;
 *
 * @endcode
 *
 * \verbatim % Result of evaluating the first function at x = 3, y = -1: 12
 % Result of evaluating the second function at x = 3, y = -1: 2
 % Result of evaluating the derivative of x of the first function at x = 3, y = -1: 7
 % Result of evaluating the derivative of y of the first function at x = 3, y = -1: 3
 % Result of evaluating the derivative of x of the second function at x = 3, y = -1: 1
 % Result of evaluating the derivative of y of the second function at x = 3, y = -1: 1 \endverbatim
 *
 * @see MultiFunction
 */
class System{


private:

    /**
     * We story the following elements inside the System class
     *  @var equations - a vector of MultiFunctions
     *  @var amount_equations - a size_t/int with the amount of equations, derived automatically from `equations` in the constructor.
     */
    std::vector<MultiFunction> equations;
    std::size_t amount_equations;

public:

    /**
    * @brief Constructor for the System class.
    */
    explicit System(const std::vector<MultiFunction>& functions);

    /**
     * @brief Getter for the amount of functions in the System.
     */
    int getAmount() const;

    /**
     * @brief Getter for the equations of all MultiFunctions in the System.
     */
    std::vector<std::string> getEquations() const;

    /**
     * @brief Getter for (partial) derivatives of all MultiFunctions in the System.
     */
    std::vector<std::vector<std::string>> getDerivatives() const;

    /**
     * @brief Getter for the equation of a specific MultiFunction in the System.
     */
    std::string getEquation(int index) const;

    /**
     * @brief Getter for the (partial) derivatives of a specific MultiFunction in the System.
     */
    std::vector<std::string> getDerivative(int index) const;

    /**
     * @brief Overloaded function call operator.
     */
    Eigen::VectorXd operator()( const Eigen::VectorXd& values);

    /**
    * @brief constructs the System's Jacobian for a given input `values` of the variables.
    */
    Eigen::MatrixXd getJacobian(const Eigen::VectorXd& values);


};
#endif //PCSC_FINAL_PROJECT_SYSTEM_HPP

