//
// Created by Anna  Proost on 22/11/2023.
//

#ifndef PCSC_FINAL_PROJECT_MULTIFUNCTION_HPP
#define PCSC_FINAL_PROJECT_MULTIFUNCTION_HPP

#include "Function.hpp"
#include "Eigen/Core"


/**
 * @brief Represents a MultiFunction (Multivariable Function) by extending the Function class to handle multivariable functions
 * (in multiple variables e.g. `x, y, z ...`).
 *
 * It includes additional functionalities and fields to save/initialise (partial) derivatives & the variable names (`x, y, z ...`)
 *
 * The getter getDerivative() returns the relevant strings.
 *
 * To evaluate the derivatives for certain variables values, the der() operator can be called. Similar to the Monofunction,
 * we overload the () operator with the values we want the function to be evaluated in.
 *
 * @code
 * // We define the main equation with 2 variables and the 2 derivative equations
 * std::string mainEquation = "x^2 - x*y";
 * std::vector<std::string> derivativeEquations = {"2 * x - y", "x"};
 * std::vector<std::string> Variables = {"x", "y"};
 *
 * MultiFunction multiFunc(mainEquation, derivativeEquations, Variables);
 *
 * // Example: Evaluate the main equation & derivatives for x = 3 and y = -1
 * Eigen::VectorXd inputValues(2);
 * inputValues << 3.0, -1.0;
 *
 * double result = multiFunc(inputValues);
 * Eigen::VectorXd result_der = multiFunc.der(inputValues);
 *
 * std::cout << "Result of evaluating main equation at x = 3, y = -1: " << result << std::endl;
 *
 * std::cout << "Result of evaluating the derivative of x at x = 3, y = -1: " << result_der[0] << std::endl;
 * std::cout << "Result of evaluating the derivative of y at x = 3, y = -1: " << result_der[1] << std::endl;
 * @endcode
 *
 * \verbatim % Result of evaluating main equation at x = 3, y = -1: 12
 % Result of evaluating the derivative of x at x = 3, y = -1: 7
 % Result of evaluating the derivative of y at x = 3, y = -1: 3 \endverbatim
 *
 * @see Function
 */
class MultiFunction : public Function {

private:

    /**
     * Besides the fields in the Function class being defined, we also store the following elements:
     *  @var derivative - all (parital) derivatives in the different variables in string form.
     *  @var der_parser - a vector of derivative parsers, that can be used to evaluate them for given values of the variables.
     *
     *  @var variables - the different variables `x, y, ...`. the order in this vector is also the order we expect the values
     *  to be in when evaluating the function/its derivatives (e.g. the first value maps to the `x`, the second to `y`, ...).
     *
     */
    std::vector<std::string> derivative;
    std::vector<mu::Parser> der_parser;

    std::vector<std::string> variables;


public:

    /**
    * @brief Constructor for the MultiFunction class.
    */
    MultiFunction(std::string eq, std::vector<std::string> der, std::vector<std::string> var);

    /**
     * @brief Getter for the (partial) derivatives.
     */
    std::vector<std::string> getDerivative() const;

    /**
     * @brief Overloaded function call operator.
     */
    double operator()(Eigen::VectorXd values);

    /**
    * @brief Computes the (partial) derivatives for a given input `values` of the variables.
    */
    Eigen::VectorXd der(Eigen::VectorXd values);

};
#endif //PCSC_FINAL_PROJECT_MULTIFUNCTION_HPP
