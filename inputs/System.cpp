#include "System.hpp"
#include "MultiFunction.hpp"
#include <muParser.h>
#include <Eigen/Dense>

    /**
    * @brief Constructor for the System class.
    *
    * Initializes a system of multivariable functions, through which all getters, ... can be accessed.
    *
    * We automatically set `amount_equations`to funct.size().
    *
    * @param funct A vector of all the (already initialized) MultiFunction instances
    */
System::System(const std::vector<MultiFunction>& funct) : equations(funct), amount_equations(funct.size()){}

/**
 * @brief Getter for the amount of functions in the System.
 *
 * This method returns the amount of MultiFunctions in the System.
 *
 * @return The amount of MultiFunctions = the length of the array used to instantiate the System instance.
 */
int System::getAmount() const { return amount_equations; }

/**
 * @brief Getter for the equations of all MultiFunctions in the System.
 *
 * This method returns all the equations in the System in std::string form, put in order into a vector.
 *
 * @return The equations of all the MultiFunctions, in the order they are in the System.
 */
std::vector<std::string> System::getEquations() const {

    std::vector<std::string> functions(amount_equations);
    for (std::size_t i = 0; i < amount_equations; ++i) { functions[i] = equations[i].getEquation(); }
    return functions;
}

/**
 * @brief Getter for (partial) derivatives of all MultiFunctions in the System.
 *
 * This method returns all the (partial) derivatives in the System in std::string form, put in order into an enscapsulating vector.
 *
 * @return The (partial) derivatives of all the MultiFunctions, in the order they are in the System.
 */
std::vector<std::vector<std::string>> System::getDerivatives() const {

    std::vector<std::vector<std::string>> derivatives(amount_equations);
    for (std::size_t i = 0; i < amount_equations; ++i) { derivatives[i] = getDerivative(i); }
    return derivatives;

}

/**
 * @brief Getter for the equation of a specific MultiFunction in the System.
 *
 * This method returns the equation of the MultiFUnction at the specified index.
 *
 * @param index the index of the MultiFunction you want the equation of.
 * @return The requested equation in string form.
 */
std::string System::getEquation(int index) const { return equations[index].getEquation(); }

/**
 * @brief Getter for the (partial) derivatives of a specific MultiFunction in the System.
 *
 * This method returns the (partial) derivatives of the MultiFUnction at the specified index
 *
 * @param index the index of the MultiFunction you want the (partial) derivatives of.
 * @return The requested (partial) derivatives, given as a vector of strings.
 */
std::vector<std::string> System::getDerivative(int index) const { return equations[index].getDerivative(); }


/**
  * @brief Overloaded function call operator.
  *
  * Evaluates the System (so all MultiFunctions) for the given values of the variables, using the eq_parsers.
  *
  * @param values The input values (Eigenvector of values).
  * @return The result of evaluating the equation in the `values` for all the MultiFunctions (Eigenvector of values).
  *
  */
Eigen::VectorXd System::operator()( const Eigen::VectorXd& values){

    Eigen::VectorXd evaluations(amount_equations);
    for (int i = 0; i < amount_equations; ++i) { evaluations[i] = equations[i](values); }
    return evaluations;
}

/**
  * @brief constructs the System's Jacobian for a given input `values` of the variables.
  *
  * This Jacobian is a square matrix, where each row represents a specific MultiFunction & has as elements the evaluation of the
  * (partial) derivatives in the inputted values.
  *
  * @param values The input values (Eigenvector of values).
  * @return The Jacobian, the result of evaluating the (partial) derivatives in the `values` for all the MultiFunctions (Square EigenMatrix of values).
  */
Eigen::MatrixXd System::getJacobian(const Eigen::VectorXd& values) {

    Eigen::MatrixXd jacobian(amount_equations, values.size());

    for (int i = 0; i < amount_equations; ++i) {
        Eigen::VectorXd equationDerivative = equations[i].der(values);

        // Copy the derivative values to the corresponding row in the Eigen matrix
        for (int j = 0; j < values.size(); ++j) {
            jacobian(i, j) = equationDerivative[j];
        }
    }

    return jacobian;
}