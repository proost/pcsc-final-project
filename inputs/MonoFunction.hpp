//
// Created by Anna  Proost on 22/11/2023.
//

#ifndef PCSC_FINAL_PROJECT_MONOFUNCTION_HPP
#define PCSC_FINAL_PROJECT_MONOFUNCTION_HPP

#include "Function.hpp"

/**
 * @brief Represents a MonoFunction (Monovariable Function) by extending the Function class to handle monovariable functions (in `x`).
 *
 * It includes additional functionalities and fields to save/initialise fixed-point equations and derivatives.
 *
 * The getters getFixedPointEquation() & getDerivative() return the relevant strings.
 *
 * To evaluate the (fixed-point) equation or derivative for a certain value of `x`, one of the operator-ish functions
 * can be called.
 *
 * @code
 * // We define the main equation, fixed-point equation, and derivative equation
 * std::string mainEquation = "x^2 - 2";
 * std::string fixedPointEquation = "2/x";
 * std::string derivativeEquation = "2 * x";
 *
 * // Create an instance of MonoFunction
 * MonoFunction monoFunc(mainEquation, fixedPointEquation, derivativeEquation);
 *
 * // Example: Evaluate the main equation & derivative for x = 3
 * double result = monoFunc(3.0);
 * double result_der = monoFunc.der(3.0);
 *
 * std::cout << "Result of evaluating main equation at x = 3: " << result << std::endl;
 *
 * std::cout << "Result of evaluating the derivative at x = 3: " << result_der << std::endl;
 * @endcode
 *
 * \verbatim % Result of evaluating main equation at x = 3: 7
 % Result of evaluating the derivative at x = 3: 6 \endverbatim
 *
 *
 * @see Function
 */
class MonoFunction : public Function {

private:

    /**
     * Besides the fields in the Function class being defined, we also store the following elements:
     *  @var fp_equation - the fixed-point equation in string form.
     *  @var fp_eq_parser - a parser of the fixed-point equation that can be used to evaluate it for a given value of `x`
     *
     *  @var derivative - the derivative (in `x`) in string form.
     *  @var der_parser - a parser of the derivative that can be used to evaluate it for a given value of `x`
     *
     */
    std::string fp_equation;
    mu::Parser fp_eq_parser;

    std::string derivative;
    mu::Parser der_parser;

public:

    /**
    * @brief Constructor for the MonoFunction class.
    *
    * Initializes a monovariable function with its main equation, fixed-point equation,
    * and derivative. The parsers for all these are also created, either within the Function() constructor call
    * or separately in the MonoFunction() constructor.
    */
    MonoFunction(const std::string& eq, const std::string& eq_fp, const std::string& der);

    /**
     * @brief Getter for the fixed-point equation.
     */
    std::string getFixedPointEquation() const;

    /**
    * @brief Getter for the derivative.
    */
    std::string getDerivative() const;


    /**
     * @brief Overloaded function call operator.
     */
    double operator()(double x);

    /**
    * @brief Computes the fixed-point equation for a given input `x`.
    */
    double fixed(double x);


    /**
    * @brief Computes the derivative for a given input `x`.
    */
    double der(double x);

};


#endif //PCSC_FINAL_PROJECT_MONOFUNCTION_HPP
