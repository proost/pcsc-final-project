#include "MonoFunction.hpp"
#include <muParser.h>


/**
 * @brief Constructor for the MonoFunction class.
 *
 * The string & parser for the function is created within the call to the Function() constructor,
 * the ones for the fixed-point equation & derivative are created in the MonoFunction() constructor.
 * In the call to the Function() constructor, we automatically set `amount_vars`to 1.
 *
 * @param eq The main equation in string form.
 * @param eq_fp The fixed-point equation in string form.
 * @param der The derivative equation in string form.
 *
 */
MonoFunction::MonoFunction(const std::string &eq, const std::string &eq_fp, const std::string &der)
    : Function(eq, 1){

    derivative = der;
    fp_equation = eq_fp;

    fp_eq_parser = mu::Parser();
    fp_eq_parser.SetExpr(eq_fp);

    der_parser = mu::Parser();
    der_parser.SetExpr(der);


}

/**
 * @brief Getter for the fixed-point equation.
 *
 * This method can be used by instances of MonoFunction
 * and returns the fixed-point equation as a std::string.
 *
 * @return The fixed-point equation in string form.
 */
std::string MonoFunction::getFixedPointEquation() const { return fp_equation; }

/**
 * @brief Getter for the derivative.
 *
 * This method can be used by instances of MonoFunction
 * and returns the derivative as a std::string.
 *
 * @return The derivative in string form.
 */
std::string MonoFunction::getDerivative() const { return derivative; }

/**
* @brief Overloaded function call operator.
*
* Given a value `x`, it evaluates the equation in the Monofunction instance for this value.
*
* @param x The input value (1 double).
* @return The result of evaluating the equation in `x` (1 double).
*/
double MonoFunction::operator()(double x) {
    eq_parser.DefineVar("x", &x);
    return eq_parser.Eval();
}

/**
* @brief Computes the fixed-point equation for a given input `x`.
*
* Given a value `x`, it evaluates the fixed-point equation in the Monofunction instance for this value.
*
* @param x The input value (1 double).
* @return The result of evaluating the fixed-point equation in `x` (1 double).
*/
double MonoFunction::fixed(double x) {
    fp_eq_parser.DefineVar("x", &x);
    return fp_eq_parser.Eval();
}

/**
* @brief Computes the derivative for a given input `x`.
*
* Given a value `x`, it evaluates the derivative in the Monofunction instance for this value.
*
* @param x The input value (1 double).
* @return The result of evaluating the derivative in `x` (1 double).
*/
double MonoFunction::der(double x) {
    der_parser.DefineVar("x", &x);
    return der_parser.Eval();
}


