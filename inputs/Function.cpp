#include "Function.hpp"
#include <muParser.h>

/**
 * @brief Constructor for the Function class, can only be accessed by instances of its daughter classes (MonoFunction & MultiFunction)
 *
 * Initializes the equation, parser, and the amount of variables for the function. The parser `eq_parser` is initialized
 * to evaluate the equation for specific values of the variable(s) later on.
 *
 * @param eq The equation in string form.
 * @param amount_vars The amount of variables in the equation.
 *
 *
 */

Function::Function(const std::string& eq, int amount_vars)
    : equation(eq), eq_parser(mu::Parser()), amount_vars(amount_vars) {

    eq_parser.SetExpr(eq);
}

/**
 * @brief Getter for the equation.
 *
 * This method can be used by instances of both subclasses (MonoFunction, MultiFunction)
 * and returns the equation as a std::string.
 *
 * @return The equation in string form.
 */

std::string Function::getEquation() const { return equation; }


/**
 * @brief Getter for the amount of variables.
 *
 * This method can be used by instances of both subclasses (MonoFunction, MultiFunction)
 * and returns the amount of vars as an int.
 *
 * @return The amount of variables (x, y, ...)
 */

int Function::getAmountVars() const { return amount_vars; }

