//
// Created by Gal on 20.11.2023.
//

#include "Newton.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <Eigen/Dense>
/**
 * @brief Constructor for the Newton class.
 *
 * Initializes the Newton method for root finding, setting the method type to Newton.
 */
Newton::Newton() : AccelerableMethod(MethodType::Newton){}
/**
 * @brief Applies the Newton method to either a single-variable function or a system of equations.
 *
 * This method uses the Newton method to find the root of a single-variable function or to solve a system
 * of equations, based on the type of the input equation. It handles both `MonoFunction` and `System` types
 * encapsulated within a `std::variant`. The starting points for the Newton method can be a single double
 * value or a vector of doubles, also provided as a `std::variant`.
 *
 * @param equation A `std::variant` encapsulating either a `MonoFunction` or a `System`.
 * @param start_points A `std::variant` holding the starting point(s) as either a double or a vector of doubles.
 * @param stop_tol The tolerance level for stopping the iteration.
 * @param max_iter The maximum number of iterations.
 * @return Solution object representing the result of the Newton method application.
 * @throws std::runtime_error If the input is neither a `MonoFunction` nor a `System`.
 */

Solution Newton::operator()(std::variant<MonoFunction,System> equation, std::variant<double, std::vector<double> > start_points, double stop_tol,unsigned int max_iter){


    if (MonoFunction* mf = std::get_if<MonoFunction>(&equation)) { // If simple function
        return mono_solve(*mf,*getDouble(start_points),stop_tol,max_iter);
    }
    else if (System* sys = std::get_if<System>(&equation)) { // If system of functions
        auto vec = *getVector(start_points);
        Eigen::VectorXd eigenVector = Eigen::Map<Eigen::VectorXd>(vec.data(), vec.size());
        return sys_solve(*sys, eigenVector, stop_tol, max_iter);
    }

    auto err = std::runtime_error("Didn't receive a MonoFunction or System");
    std::cerr<<err.what();
    throw err;
}

/**
 * @brief Solves a single-variable equation using the Newton method.
 *
 * This method applies the Newton method to find the root of a non-linear single-variable equation.
 * The equation should be provided as a function taking a double and returning a double.
 *
 * @param [in] equation The single-variable equation to be solved.
 * @return The root of the equation as a double.
 */
Solution Newton::mono_solve(MonoFunction equation, double start, double stop_tol,unsigned int max_iter) {

    std::vector<double> x; //Vector of the iterations
    x.push_back(start);

    bool converged=false;
    unsigned int i;

    for(i=1;i<=max_iter;i++){ // While didn't converge and under max iterations

        try{
            x.push_back(this->step(equation,x.back())); // Iterate one step
        }
        catch(const std::runtime_error& e ){ // If wrong input
            std::cerr << "Exception caught: " << e.what() << std::endl;
            return Solution(x,false,type);
        }
        converged = std::abs(equation(x[i])) < stop_tol; //Checks if converged
        if (converged) {return Solution(x,converged,type);}
    }

    return Solution(x,converged,type);
}
/**
 * @brief Performs a single step of the Newton method for single variable functions.
 *
 * Computes the next approximation of the root using the current iteration value and the function's derivative.
 * @param equation The function to be solved.
 * @param x The current iteration value.
 * @return The next approximation of the root.
 */
double Newton::step(MonoFunction &equation, double x){ // Does one iteration

    double der = equation.der(x);
    if(der==0){ // To avoid division by 0
        auto err = std::runtime_error("Derivative equal to 0");
        std::cerr<<err.what();
        throw err;
    }
    return x - equation(x)/der;
}
/**
 * @brief Implements the Newton method for solving systems of equations.
 *
 * This method extends the Newton-Raphson approach to systems, iteratively refining an initial guess vector until the solution converges within the specified tolerance or the maximum number of iterations is reached.
 * @param sys The system of equations to be solved.
 * @param start_points The starting points as an Eigen::VectorXd.
 * @param stop_tol The stopping tolerance for the method.
 * @param max_iter The maximum number of iterations.
 * @return Solution object containing the results of the Newton method for systems.
 */
Solution Newton::sys_solve( System& sys, Eigen::VectorXd start_points, double stop_tol, unsigned int max_iter) {

    std::vector<Eigen::VectorXd> iterations;
    if ( sys.getJacobian(start_points).determinant() == 0 ) {start_points[0] += 1e-2;}
    iterations.push_back(start_points);

    bool converged = false;

    for(int i=0;i<max_iter && !converged;i++){

        Eigen::MatrixXd A = sys.getJacobian(iterations.back()); // J(x_n)

        double det = A.determinant();
        if(det==0){ // No solution
            auto err = std::runtime_error("Derivative equal to 0");
            std::cerr<<err.what();
            throw err;
        }

        Eigen::VectorXd b = -sys(iterations.back()); // -F(x_n)
        Eigen::VectorXd dx = A.lu().solve(b); // Solve J(x_n)*(x_n+1 - x_n) = -F(x_n)
        Eigen::VectorXd next = dx  + iterations.back(); // x_n+1 = (x_n+1 -x_n) + x_n
        iterations.push_back(next); // Add it to the sequence

        converged = sys(next).norm() < stop_tol; // Check convergence
    }

    return Solution(iterations,converged,MethodType::NewtonMulti);
}

