//
// Created by Gal on 23.11.2023.
//
#include "Method.hpp"
#include "../inputs/MonoFunction.hpp"

#ifndef PCSC_FINAL_PROJECT_ACCELERABLEMETHOD_H
#define PCSC_FINAL_PROJECT_ACCELERABLEMETHOD_H

/**
 * @brief The AccelerableMethod class, derived from Method, serves as a base class for numerical methods that can be accelerated.
 *
 * It introduces virtual functions for initialization and performing a step in the numerical method, which are to be implemented by derived classes.
 */
class AccelerableMethod: public Method {

public:
    /**
 * @brief Constructor for the AccelerableMethod class.
 */
    AccelerableMethod(MethodType type);

protected:
    friend class Aitken;///< Allows the Aitken class to access protected members of AccelerableMethod.

    /**
 * @brief Pure virtual function for performing a single step of the method.
 *
 * This function needs to be implemented by derived classes to define the procedure of a single iteration step.
 * @param equation The function to be solved (MonoFunction).
 * @param x The current iteration value.
 * @return The result of the step as a double.
 */
    virtual double step(MonoFunction &equation,double x)=0;
};


#endif //PCSC_FINAL_PROJECT_ACCELERABLEMETHOD_H
