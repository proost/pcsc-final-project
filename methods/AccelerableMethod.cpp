//
// Created by Gal on 23.11.2023.
//

#include "AccelerableMethod.hpp"

/**
 * @brief Constructor for the AccelerableMethod class.
 *
 * This constructor initializes the AccelerableMethod with the specified method type, delegating to the base class (Method) constructor.
 * @param type The type of numerical method (MethodType enumeration).
 */
AccelerableMethod::AccelerableMethod(MethodType type) : Method(type){}


