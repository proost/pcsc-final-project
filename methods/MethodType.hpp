//
// Created by Gal on 06.12.2023.
//

#ifndef PCSC_FINAL_PROJECT_METHODTYPE_HPP
#define PCSC_FINAL_PROJECT_METHODTYPE_HPP

enum class MethodType {
    Newton,
    Bisection,
    Chord,
    FixedPoint,
    Aitken,
    NewtonMulti,
};
#endif //PCSC_FINAL_PROJECT_METHODTYPE_HPP
