//
// Created by Gal on 20.11.2023.
//

#ifndef PCSC_FINAL_PROJECT_CHORD_HPP
#define PCSC_FINAL_PROJECT_CHORD_HPP


#include "Method.hpp"
#include "../inputs/MonoFunction.hpp"
#include "AccelerableMethod.hpp"
/**
 * @brief The Chord class, derived from Method, implements the Chord method (also known as the False Position method) for finding roots of a function.
 *
 * This method is similar to the Bisection method but uses a linear interpolation to guess the root, potentially providing faster convergence.
 */
class Chord: public Method {
public:
/**
 * @brief Constructor for the Chord class.
 *
 * Initializes a new instance of the Chord method.
 */
    Chord();
/**
 * @brief Overloads the function call operator to apply the Chord method for finding roots.
 */
    Solution operator()(std::variant<MonoFunction,System> equation, std::variant<double, std::vector<double> > start, double stop_tol, unsigned int max_iter) override;
private:
    /**
    * @brief Private function to perform a single step of the Chord method.
    */
    double step(MonoFunction &equation, std::vector<double> &x);
};


#endif //PCSC_FINAL_PROJECT_CHORD_HPP
