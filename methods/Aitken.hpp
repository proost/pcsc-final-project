//
// Created by Gal on 23.11.2023.
//

#ifndef PCSC_FINAL_PROJECT_AITKEN_HPP
#define PCSC_FINAL_PROJECT_AITKEN_HPP

#include "Method.hpp"
#include "AccelerableMethod.hpp"

/**
 * @brief The Aitken class, derived from Method, implements the Aitken's delta-squared process for accelerating the convergence of a given iterative method.
 */
class Aitken: public Method {

public:
    /**
 * @brief Constructor for the Aitken class.
 */
    Aitken(AccelerableMethod *method);

/**
 * @brief Overloads the function call operator to apply the Aitken acceleration process to the provided numerical method.
 */
    Solution operator()(std::variant<MonoFunction,System> equation, std::variant<double, std::vector<double> > start_points, double stop_tol,unsigned int max_iter) override;
private:
    AccelerableMethod *method;///< Pointer to the AccelerableMethod to be accelerated.
};


#endif //PCSC_FINAL_PROJECT_AITKEN_HPP
