//
// Created by Gal on 20.11.2023.
//

#ifndef PCSC_FINAL_PROJECT_NEWTON_HPP
#define PCSC_FINAL_PROJECT_NEWTON_HPP


#include <vector>
#include "AccelerableMethod.hpp"
#include "Aitken.hpp"
#include "../inputs/MonoFunction.hpp"
#include "Eigen/Core"
/**
 * @brief The Newton class, derived from AccelerableMethod, implements the Newton-Raphson method for finding roots of single-variable functions and can also be extended to systems of equations.
 *
 * This method uses an iterative approach, requiring an initial guess and using the function's derivative for refinement.
 */
class Newton: public AccelerableMethod {
public:
    /**
 * @brief Constructor for the Newton class.
 *
 * Initializes a new instance of the Newton method.
 */
    Newton();
/**
 * @brief Applies the Newton method to either a single-variable function or a system of equations.
 *
 * This method uses the Newton method to find the root of a single-variable function or to solve a system
 * of equations, based on the type of the input equation. It handles both `MonoFunction` and `System` types
 * encapsulated within a `std::variant`. The starting points for the Newton method can be a single double
 * value or a vector of doubles, also provided as a `std::variant`.
 * */
    Solution operator()(std::variant<MonoFunction,System> equation, std::variant<double, std::vector<double> > start_points, double stop_tol,unsigned int max_iter) override;
    /**
 * @brief Overloads the function call operator to apply the Newton method for systems of equations.
 */

private:
    friend class Aitken;///< Allows the Aitken class to access protected members of Newton.
    /**
 * @brief Private function to perform a single step of the Newton method.
 */
    double step(MonoFunction &equation, double x) override;
/**
* @brief Implements the Newton method for solving systems of equations.
*/
Solution sys_solve(System& sys, Eigen::VectorXd start_points, double stop_tol, unsigned int max_iter);
/**
* @brief Implements the Newton method for one simple equation.
*/
Solution mono_solve(MonoFunction equation, double start_point, double stop_tol, unsigned int max_iter);
};


#endif //PCSC_FINAL_PROJECT_NEWTON_HPP
