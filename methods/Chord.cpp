//
// Created by Gal on 20.11.2023.
//

#include "Chord.hpp"
#include <cmath>
#include <iostream>

/**
 * @brief Constructor for the Chord class.
 *
 * Initializes a new instance of the Chord method, setting the method type to Chord.
 */
Chord::Chord() : Method(MethodType::Chord){}
/**
 * @brief Implements the Chord method for finding the root of a function.
 *
 * This method uses linear interpolation to estimate the root in an interval where the function changes sign. It iteratively refines this estimate until the root is found within the specified tolerance or the maximum number of iterations is reached.
 * @param equ A variant encapsulating either a MonoFunction or a System to be solved.
 * @param start A variant holding the starting interval as either a double or a pair of doubles in a vector.
 * @param stop_tol The stopping tolerance for the method.
 * @param max_iter The maximum number of iterations.
 * @return Solution object containing the results of the Chord method.
 */
Solution Chord::operator()(std::variant<MonoFunction,System> equ, std::variant<double, std::vector<double> > start, double stop_tol,unsigned int max_iter) {

    std::vector<double> x; //Vector of the iterations
    MonoFunction equation = *getFunction(equ); // Checks variant
    auto pair = *getVector(start); //Checks variant
    x.push_back(pair[0]);
    x.push_back(pair[1]);

    bool converged=false;
    unsigned int i;

    for(i=1;i <= max_iter;i++){ //Keep looping while under max iteration and not converged
        double x_past = x.at(x.size() -2);
        x.push_back(this->step(equation,x)); // iterate

        converged = std::abs(equation(x.back())) < stop_tol; //Checks if converged
        if (converged) {return Solution(x,converged,type);}

    }
    return Solution(x,converged,type);
}
/**
 * @brief Performs a single step of the Chord method.
 *
 * Computes the next approximation of the root using linear interpolation based on the current iteration values.
 * @param f The function to be solved (MonoFunction).
 * @param x A vector of doubles representing the current iteration values.
 * @return The next approximation of the root as a double.
 */
double Chord::step(MonoFunction &f, std::vector<double> &x){
    double x_past = x.at(x.size() -2);
    double xn = x.back();
    return (xn - (xn - x_past) / (f(xn) - f(x_past)) * f(xn)); // xn+1 = (xn - xn-1) / ( f(xn) -f(xn-1) ) * f(xn)
}
