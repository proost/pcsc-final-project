//
// Created by Gal on 20.11.2023.
//

#ifndef PCSC_FINAL_PROJECT_METHOD_HPP
#define PCSC_FINAL_PROJECT_METHOD_HPP

#include<variant>
#include <vector>
#include <utility>
#include "../outputs/Solution.hpp"
#include "../inputs/Function.hpp"
#include "../inputs/System.hpp"
#include "../inputs/MonoFunction.hpp"

/**
 * @brief The Method class serves as a base class for various numerical methods.
 *
 * It provides a common interface for different numerical solution methods and stores the method type.
 */
class Method {

public:
    /**
 * @brief Constructor for the Method class.
 */
    Method(MethodType type);

    /**
 * @brief Pure virtual function operator to be implemented by derived classes.
 *
 * This operator defines how each numerical method operates on a given function, start points, tolerance, and maximum iterations.
 * @param equation The function to be solved (MonoFunction).
 * @param start_points A variant that can be either a double for methods requiring a single initial guess or a pair of doubles for methods requiring an interval.
 * @param stop_tol The stopping tolerance for the method.
 * @param max_iter The maximum number of iterations.
 * @return Solution object containing the results of the numerical method.
 */
    virtual Solution operator()(std::variant<MonoFunction,System> equation, std::variant<double, std::vector<double> > start_points, double stop_tol,unsigned int max_iter)=0;
/**
* @brief Creates and returns a pointer to a numerical method object.
*
* Depending on the method name and whether Aitken's acceleration is used, this function
* creates an appropriate numerical method object and returns its pointer.
*/
static Method* getMethod(const std::string& m,bool aitken);
protected:
/**
* @brief Extracts a vector of doubles from a variant.
*
* Attempts to extract a vector of doubles from a std::variant that also holds a double. Throws
* an exception if the variant does not contain a vector of doubles.
*/
static std::vector<double>* getVector(std::variant<double,std::vector<double>> start_points);
/**
 * @brief Extracts a double from a variant.
 *
 * Attempts to extract a double from a std::variant that also holds a vector of doubles. Throws
 * an exception if the variant does not contain a double.
 */
static double* getDouble(std::variant<double,std::vector<double>> start_points);
/**
 * @brief Extracts a MonoFunction from a variant.
 *
 * Attempts to extract a MonoFunction from a std::variant that also holds System. Throws
 * an exception if the variant does not contain a MonoFunction.
 */
static MonoFunction* getFunction(std::variant<MonoFunction,System> equation);
/**
 * @brief Extracts a System from a variant.
 *
 * Attempts to extract a System from a std::variant that also holds MonoFunction. Throws
 * an exception if the variant does not contain a System.
 */
static System* getSystem(std::variant<MonoFunction,System> equation);
MethodType type;///< The type of numerical method used (MethodType enumeration).
};


#endif //PCSC_FINAL_PROJECT_METHOD_HPP
