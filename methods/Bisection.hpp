//
// Created by Gal on 20.11.2023.
//

#ifndef PCSC_FINAL_PROJECT_BISECTION_HPP
#define PCSC_FINAL_PROJECT_BISECTION_HPP


#include "Method.hpp"
#include "../inputs/Function.hpp"
/**
 * @brief The Bisection class, derived from Method, implements the Bisection algorithm for finding roots of a function.
 *
 * This method is used to find the root of a function in a specified interval where the function changes sign.
 */
class Bisection: public Method {

public:
/**
 * @brief Constructor for the Bisection class.
 */
    Bisection();
/**
 * @brief Overloads the function call operator to apply the Bisection method for finding roots.
 */
    Solution operator()(std::variant<MonoFunction,System> equation, std::variant<double, std::vector<double> > start_points, double stop_tol,unsigned int max_iter) override;


};


#endif //PCSC_FINAL_PROJECT_BISECTION_HPP
