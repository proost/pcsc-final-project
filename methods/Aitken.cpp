//
// Created by Gal on 23.11.2023.
//

#include "Aitken.hpp"
#include<iostream>
#include<cmath>
/**
 * @brief Constructor for the Aitken class.
 *
 * Initializes the Aitken method with the specified AccelerableMethod.
 * @param method Pointer to an AccelerableMethod object that will be accelerated by Aitken's method.
 */
Aitken::Aitken(AccelerableMethod *method): Method(MethodType::Aitken),method(method) {}
/**
 * @brief Implements the Aitken acceleration process on the provided numerical method.
 *
 * This function applies Aitken's delta-squared process to accelerate the convergence of the provided numerical method.
 * It iteratively calls the method's step function and applies the acceleration process until convergence or the maximum iterations are reached.
 * @param equ A variant encapsulating either a MonoFunction or a System to be solved.
 * @param start_points A variant holding the starting points, either a single double for an initial guess or a vector of doubles for interval methods.
 * @param stop_tol The stopping tolerance for the Aitken method.
 * @param max_iter The maximum number of iterations.
 * @return Solution object containing the results of the accelerated method.
 */
Solution Aitken::operator()(std::variant<MonoFunction,System> equ, std::variant<double, std::vector<double> > start_points, double stop_tol, unsigned int max_iter){

    MonoFunction equation = *getFunction(equ); // Makes sure that we get a MonoFunction as an equation.
    double start= *getDouble(start_points); // Makes sure we get a double as input

    std::vector<double> x ={start};
    std::vector<double> ax ={start};

    bool converged=false;

    for(int i=1;i <= max_iter+2 && !converged; i++){

        x.push_back(method->step(equation,x.back()));
        if(i >= 3){

            auto xn2 = x.back();
            auto xn1 = x[x.size()-2];
            auto xn = x[x.size()-3];

            ax.push_back(xn - ((xn1 - xn)*(xn1 - xn)) / (xn2 - 2 * xn1 + xn));
        }
        converged = std::abs(equation(ax.back())) < stop_tol;

    }

    return Solution(ax,converged,method->type);

}

