//
// Created by Gal on 20.11.2023.
//

#include "Bisection.hpp"
#include "../inputs/Function.hpp"
#include <cmath>
#include <iostream>

/**
 * @brief Implements the Bisection method for finding the root of a function.
 *
 * This method narrows down the interval [a, b] iteratively, where the function changes sign, to find the root. The interval is bisected at each step, and the half where the sign change occurs is selected for the next iteration. The process continues until the function value at the midpoint is close enough to zero (within stop_tol), or the maximum number of iterations (max_iter) is reached.
 * @param equ A variant encapsulating either a MonoFunction or a System to be solved.
 * @param start_points A variant holding the starting interval as either a double or a pair of doubles in a vector.
 * @param stop_tol The stopping tolerance for the method.
 * @param max_iter The maximum number of iterations.
 * @return Solution object containing the results of the Bisection method.
 */
Solution Bisection::operator()(std::variant<MonoFunction,System> equ, std::variant<double, std::vector<double>> start_points, double stop_tol, unsigned int max_iter) {

    MonoFunction equation = *getFunction(equ); //Checks the variant
    auto start_point = *getVector(start_points); // Checks the variant
    std::vector<std::pair<double,double>> iterations;

    iterations.push_back(std::pair<double,double>(start_point[0],start_point[1]));
    double m;
    bool converged = false;
    unsigned int i;

    for(i=0;i <= max_iter; i++){

        double a = iterations.back().first;
        double b = iterations.back().second;
        m = (a+b)/2;

        if(equation(a)*equation(m) <= 0){
            iterations.push_back(std::pair<double,double>(a,m)); // b=m
        }
        else{
            iterations.push_back(std::pair<double,double>(m,b)); // a=m
        }
        converged = std::abs(equation(m)) < stop_tol; // Checks convergence
        if (converged) {return Solution(iterations,converged,type);}
    }
    return Solution(iterations,converged,type);
}

/**
 * @brief Constructor for the Bisection class.
 *
 * Initializes a new instance of the Bisection method, setting the method type to Bisection.
 */
Bisection::Bisection() : Method(MethodType::Bisection){}

