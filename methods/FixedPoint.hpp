//
// Created by Gal on 21.11.2023.
//

#ifndef PCSC_FINAL_PROJECT_FIXEDPOINT_HPP
#define PCSC_FINAL_PROJECT_FIXEDPOINT_HPP


#include "Method.hpp"
#include "../inputs/MonoFunction.hpp"
#include "AccelerableMethod.hpp"
/**
 * @brief The FixedPoint class, derived from AccelerableMethod, implements the Fixed Point Iteration method for finding roots of a function.
 *
 * This method is used to find a fixed point of a function, which is also a root of the function f(x) = x - g(x), where g(x) is the iteration function.
 */
class FixedPoint: public AccelerableMethod {
public:
/**
 * @brief Constructor for the FixedPoint class.
 */
    FixedPoint();

    /**
    * @brief Overloads the function call operator to apply the Fixed Point Iteration method for finding roots.
    */
    Solution operator()(std::variant<MonoFunction,System> equation, std::variant<double, std::vector<double> > start_points, double stop_tol, unsigned int max_iter) override;

private:
    friend class Aitken;
/**
 * @brief Private function to perform a single step of the Fixed Point Iteration method.
 */
    double step(MonoFunction &equation,double x) override;
};


#endif //PCSC_FINAL_PROJECT_FIXEDPOINT_HPP
