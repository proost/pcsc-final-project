//
// Created by Gal on 21.11.2023.
//

#include "FixedPoint.hpp"
#include <cmath>
#include <iostream>
/**
 * @brief Constructor for the FixedPoint class.
 *
 * Initializes a new instance of the Fixed Point Iteration method, setting the method type appropriately.
 */
FixedPoint::FixedPoint() : AccelerableMethod(type){}
/**
 * @brief Implements the Fixed Point Iteration method for finding the root of a function.
 *
 * This method iteratively applies the fixed point iteration formula using the given starting point, refining the estimate until the root is found within the specified tolerance or the maximum number of iterations is reached.
 * @param equ A variant encapsulating either a MonoFunction or a System to be solved.
 * @param start_points A variant holding the starting point(s) as either a double or a vector of doubles.
 * @param stop_tol The stopping tolerance for the method.
 * @param max_iter The maximum number of iterations.
 * @return Solution object containing the results of the Fixed Point Iteration method.
 */
Solution FixedPoint::operator()(std::variant<MonoFunction,System> equ, std::variant<double, std::vector<double> > start_points, double stop_tol, unsigned int max_iter) {

    std::vector<double> x; //Vector of the iterations
    MonoFunction equation("","","");
    //Checks variants
    x.push_back(*getDouble(start_points)); // start points
    equation = *getFunction(equ);  // equation


    bool converged=false;
    unsigned int i;
    for(i=1;i<=max_iter;i++){ // While didn't converge and under max iterations

        x.push_back(this->step(equation,x.back())); //Adds next step
        converged = std::abs(equation(x[i])) < stop_tol; //Checks if converged
        if (converged) {return Solution(x,converged,type);}
    }

    return Solution(x,converged,type);
}
/**
 * @brief Performs a single step of the Fixed Point Iteration method.
 *
 * Computes the next approximation of the root using the fixed point iteration function.
 * @param f The function to be solved (MonoFunction).
 * @param x The current iteration value.
 * @return The next approximation of the root as a double.
 */
double FixedPoint::step(MonoFunction &f,double x){
    return f.fixed(x);
}
