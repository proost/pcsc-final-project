//
// Created by Gal on 20.11.2023.
//

#include "headers.hpp"
#include "Method.hpp"

#include <cmath>
/**
 * @brief Constructor for the Method class.
 *
 * Initializes the method with the specified type.
 * @param type The type of numerical method (MethodType enumeration).
 */
Method::Method(MethodType type) : type(type){

}
/**
 * @brief Creates and returns a pointer to a numerical method object.
 *
 * Depending on the method name and whether Aitken's acceleration is used, this function
 * creates an appropriate numerical method object and returns its pointer.
 *
 * @param method_name The name of the numerical method to create.
 * @param aitken Indicates whether to use Aitken's acceleration.
 * @return Pointer to the created Method object.
 * @throws std::runtime_error if an invalid method name is provided or Aitken is not available.
 */
Method *Method::getMethod(const std::string& method_name,bool aitken) {

    if (aitken) {
        if (method_name == "N")
            return new Aitken(new Newton());
        else if (method_name == "FP")
            return new Aitken(new FixedPoint());
        else{
            auto err = std::runtime_error("Aitken not available for this method");
            std::cerr<< err.what();
            throw err;
        }
    }

    if (method_name == "N" || method_name == "SN")
        return new Newton();
    else if (method_name == "C")
        return new Chord();
    else if (method_name == "FP")
        return new FixedPoint();
    else if (method_name == "B")
        return new Bisection();
    else{
        auto err = std::runtime_error("Wrong method name");
        std::cerr<<err.what();
        throw err;
    }

}

/**
 * @brief Extracts a MonoFunction from a variant.
 *
 * Attempts to extract a MonoFunction from a std::variant that also holds System. Throws
 * an exception if the variant does not contain a MonoFunction.
 *
 * @param equation The std::variant containing either MonoFunction or System.
 * @return Pointer to the extracted MonoFunction.
 * @throws std::runtime_error if the variant does not contain a MonoFunction.
 */
MonoFunction* Method::getFunction(std::variant<MonoFunction, System> equation) {
    auto monofunc = std::get_if<MonoFunction>(&equation); // Try to extract the MonoFunction

    if (monofunc==nullptr) { // Check if the variant contains a MonoFunction
        auto err = std::runtime_error("Didn't get a MonoFunction");
        std::cerr<<err.what();
        throw err; // Throws an exception if incorrect format
    }

    return monofunc;
}
/**
 * @brief Extracts a System from a variant.
 *
 * Attempts to extract a System from a std::variant that also holds MonoFunction. Throws
 * an exception if the variant does not contain a System.
 *
 * @param equation The std::variant containing either MonoFunction or System.
 * @return Pointer to the extracted System.
 * @throws std::runtime_error if the variant does not contain a System.
 */
System* Method::getSystem(std::variant<MonoFunction, System> equation) {
    auto sys = std::get_if<System>(&equation); // Try to extract the MonoFunction

    if (sys==nullptr) { // Check if the variant contains a MonoFunction
        auto err = std::runtime_error("Didn't get a System");
        std::cerr<<err.what();
        throw err; // Throws an exception if incorrect format
    }

    return sys;
}
/**
 * @brief Extracts a vector of doubles from a variant.
 *
 * Attempts to extract a vector of doubles from a std::variant that also holds a double. Throws
 * an exception if the variant does not contain a vector of doubles.
 *
 * @param start_points The std::variant containing either a double or a vector of doubles.
 * @return Pointer to the extracted vector of doubles.
 * @throws std::runtime_error if the variant does not contain a vector of doubles.
 */
std::vector<double> *Method::getVector(std::variant<double, std::vector<double>> start_points) {

    auto start_point = std::get_if<std::vector<double> >(&start_points);
    if (start_point==nullptr) { // Check if the variant contains a pair of starting points
        auto err = std::runtime_error("Didn't get a std::vector<double> ");
        std::cerr<<err.what();
        throw err;
    }
    return start_point;
}
/**
 * @brief Extracts a double from a variant.
 *
 * Attempts to extract a double from a std::variant that also holds a vector of doubles. Throws
 * an exception if the variant does not contain a double.
 *
 * @param start_points The std::variant containing either a double or a vector of doubles.
 * @return Pointer to the extracted double.
 * @throws std::runtime_error if the variant does not contain a double.
 */
double *Method::getDouble(std::variant<double, std::vector<double>> start_points) {

    auto start_point = std::get_if<double>(&start_points);
    if (start_point==nullptr) { // Check if the variant contains a double
        auto err = std::runtime_error("Didn't get a double ");
        std::cerr<<err.what();
        throw err;
    }
    return start_point;
}
