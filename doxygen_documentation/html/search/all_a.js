var searchData=
[
  ['method_0',['method',['../class_method.html',1,'Method'],['../class_aitken.html#ab4db6e388b1e44f45d3b9b14d036485f',1,'Aitken::method'],['../class_method.html#a7c0834d8219cdac577e47b290b535018',1,'Method::Method()']]],
  ['method_20tests_1',['Method tests',['../md__r_e_a_d_m_e.html#autotoc_md6',1,'']]],
  ['methods_20for_20the_20solution_20of_20nonlinear_20equations_2',['Implementation of numerical methods for the solution of nonlinear equations.',['../md__r_e_a_d_m_e.html',1,'']]],
  ['mono_5fsolve_3',['mono_solve',['../class_newton.html#a55102f95f37640dc54462ff710f84370',1,'Newton']]],
  ['monofunction_4',['monofunction',['../class_mono_function.html',1,'MonoFunction'],['../class_mono_function.html#aa77ef273bf7c175a502516706d8d6acf',1,'MonoFunction::MonoFunction()']]],
  ['multifunction_5',['multifunction',['../class_multi_function.html',1,'MultiFunction'],['../class_multi_function.html#a9de47a95a7051f6700926be3477933e9',1,'MultiFunction::MultiFunction()']]]
];
