var searchData=
[
  ['tests_0',['tests',['../md__r_e_a_d_m_e.html#autotoc_md18',1,'Converging Tests'],['../md__r_e_a_d_m_e.html#autotoc_md15',1,'Converging Tests'],['../md__r_e_a_d_m_e.html#autotoc_md12',1,'Converging Tests'],['../md__r_e_a_d_m_e.html#autotoc_md8',1,'Converging Tests'],['../md__r_e_a_d_m_e.html#autotoc_md9',1,'Diverging Tests'],['../md__r_e_a_d_m_e.html#autotoc_md16',1,'Diverging Tests'],['../md__r_e_a_d_m_e.html#autotoc_md13',1,'Diverging Tests'],['../md__r_e_a_d_m_e.html#autotoc_md19',1,'Diverging Tests'],['../md__r_e_a_d_m_e.html#autotoc_md6',1,'Method tests'],['../md__r_e_a_d_m_e.html#autotoc_md10',1,'System Tests'],['../md__r_e_a_d_m_e.html#autotoc_md5',1,'Tests']]],
  ['the_20solution_20of_20nonlinear_20equations_1',['Implementation of numerical methods for the solution of nonlinear equations.',['../md__r_e_a_d_m_e.html',1,'']]],
  ['to_20compile_2',['How to compile',['../md__r_e_a_d_m_e.html#autotoc_md1',1,'']]],
  ['to_20launch_20doxygen_3',['How to launch Doxygen',['../md__r_e_a_d_m_e.html#autotoc_md22',1,'']]],
  ['todos_20and_20problems_4',['Limitations, Perspectives &amp; TODOs, and Problems',['../md__r_e_a_d_m_e.html#autotoc_md23',1,'']]],
  ['type_5',['type',['../class_solution.html#a1897affd3f3a3dd3ce95ae04b9d1c5aa',1,'Solution::type'],['../class_method.html#ae315d91164b622836160e0666173c790',1,'Method::type']]]
];
