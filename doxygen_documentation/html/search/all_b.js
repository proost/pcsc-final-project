var searchData=
[
  ['newton_0',['newton',['../class_newton.html',1,'Newton'],['../class_newton.html#aa28a68d400a6457328e8554f2c00ff13',1,'Newton::Newton()']]],
  ['newtonmethod_1',['NewtonMethod',['../class_solver.html#a64b719c61f4321e66774d87946f007be',1,'Solver']]],
  ['newtonmethoda_2',['NewtonMethodA',['../class_solver.html#a887d0bedba17c8173c2d3ca926b588cf',1,'Solver']]],
  ['newtontest_3',['NewtonTest',['../md__r_e_a_d_m_e.html#autotoc_md7',1,'']]],
  ['nonlinear_20equations_4',['Implementation of numerical methods for the solution of nonlinear equations.',['../md__r_e_a_d_m_e.html',1,'']]],
  ['numerical_20methods_20for_20the_20solution_20of_20nonlinear_20equations_5',['Implementation of numerical methods for the solution of nonlinear equations.',['../md__r_e_a_d_m_e.html',1,'']]]
];
