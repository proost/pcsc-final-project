var searchData=
[
  ['solution_0',['solution',['../class_solution.html#a87abff475c54b6a8ef4b115d8928c192',1,'Solution::Solution(std::vector&lt; double &gt; iterations, bool converged, MethodType type)'],['../class_solution.html#ad48d50a1172938dc6011b20da692a3c6',1,'Solution::Solution(std::vector&lt; std::pair&lt; double, double &gt; &gt; iterations, bool converged, MethodType type)'],['../class_solution.html#af65308c40949272c9c559122b9c65e41',1,'Solution::Solution(std::vector&lt; Eigen::VectorXd &gt; iterations, bool converged, MethodType type)']]],
  ['solve_1',['Solve',['../class_solver.html#a08b4be0d05b96c8ea4e73400f30b63c7',1,'Solver']]],
  ['step_2',['step',['../class_accelerable_method.html#a603556ebe275f5d6e6d9cf757c081e75',1,'AccelerableMethod::step()'],['../class_chord.html#a239e2b5a0046722218e5b22d844b30d2',1,'Chord::step()'],['../class_fixed_point.html#aeaf2c41acae806f3668fe1c0fefe5989',1,'FixedPoint::step()'],['../class_newton.html#a4b464d8d11b0483d1cf19a0f1703e537',1,'Newton::step()']]],
  ['stringvector_3',['stringVector',['../class_solution.html#ae19212259516fcaf16942eaac02ec404',1,'Solution']]],
  ['sys_5fsolve_4',['sys_solve',['../class_newton.html#affc0128a149331cd5d698a9d50ad9e46',1,'Newton']]],
  ['system_5',['System',['../class_system.html#afc4fcf375baa8e3c25693cf772b57a4f',1,'System']]],
  ['systemnewtonmethod_6',['SystemNewtonMethod',['../class_solver.html#a984b89dede2d8eee892429fad01f70a3',1,'Solver']]]
];
